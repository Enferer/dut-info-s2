import tps.Telephone;

class EssaiTelephone{
	public static void main(String[]args){
		System.out.println( new Telephone(0,0,0,0,0) );
		System.out.println( new Telephone(11,11,11,11,11) );
		System.out.println( new Telephone(22,22,22,22,22) );		
		System.out.println( new Telephone(33,33,33,33,33) );	
		System.out.println( new Telephone(44,44,44,44,44) );
	} 
}