import java.util.HashMap;

import javax.swing.JOptionPane;

public class Bulletin {

	HashMap<String,int[]> m1;
	
	public Bulletin(){
		m1 = new HashMap<String,int[]>();
	}
	public Bulletin(String n, int[]tab){
		m1 = new HashMap<String,int[]>();
		m1.put(n, tab);
	}
	public void Ajouter(String n, int[]tab){
		m1.put(n, tab);
	}

	void afficher(){
		String etu = "";
		do{
			etu = JOptionPane.showInputDialog("Rentrez un étudiant");
		}while(!m1.containsKey(etu));
		
		String note = "Les note de "+etu+" sont :";
		int[] tab = m1.get(etu);
		for (int i = 0; i < tab.length; i++) {
			note += tab[i]+", ";
		}
		JOptionPane.showMessageDialog(null,note);
		
	}
	void afficher(String n){
		while(!m1.containsKey(n)){
			n = JOptionPane.showInputDialog("Rentrez un étudiant");
		}
		
		String note = "Les note de "+n+" sont :";
		int[] tab = m1.get(n);
		for (int i = 0; i < tab.length; i++) {
			note += tab[i]+", ";
		}
		JOptionPane.showMessageDialog(null,note);
		
	}
	
	/*public static void main(String[] args) {
		Bulletin b1 = new Bulletin();
		b1.Ajouter("pierre",new int[]{12,13,14,15,16});
		b1.Ajouter("paul",new int[]{13,13,14,15,16});
		b1.Ajouter("jacque",new int[]{13,17,14,15,16});
		b1.Ajouter("vincent",new int[]{10,13,14,15,16});
		b1.Ajouter("francois",new int[]{13,17,14,12,16});
		b1.afficher();
	}*/

}
