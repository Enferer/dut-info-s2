package tp2bis;

import java.util.ArrayList;
import java.util.List;
//import tps.Collector;

public class Catalogue {
    private String nom;
    private List<Collector> elts;

    public Catalogue(String nom) {
        this.nom = nom;
        this.elts = new ArrayList<Collector>();
    }

    public String toString() {
        StringBuilder ch = new StringBuilder();
        ch.append("\nCatalogue: ").append(this.nom).append("\n");
        for (Collector coll : this.elts) {
            ch.append(" - ").append((Object)coll).append("\n");
        }
        return ch.toString();
    }

    public int size() {
        return this.elts.size();
    }

    public Collector get(String identifiant) {
        for (Collector coll : this.elts) {
            if (!identifiant.equals(coll.getIdentifiant())) continue;
            return coll;
        }
        return null;
    }

    public void ajouter(String identifiant, int nbExemplaires) {
        Collector coll = this.get(identifiant);
        if (coll == null) {
            coll = new Collector(identifiant);
            this.elts.add(coll);
        }
        coll.ajouter(nbExemplaires);
    }

    public void ajouter(String identifiant) {
        this.ajouter(identifiant, 0);
    }

    public boolean supprimer(String identifiant) {
        Collector coll = this.get(identifiant);
        if (coll == null || coll.getNbExemplaires() > 0) {
            return false;
        }
        return this.elts.remove((Object)coll);
    }

    public int enlever(int nbExemplaires, String identifiant) {
        Collector coll = this.get(identifiant);
        if (coll == null) {
            return 0;
        }
        return coll.suppression(nbExemplaires);
    }

    public boolean changerPrix(String identifiant, int prix) {
        Collector coll = this.get(identifiant);
        if (coll == null) {
            return false;
        }
        coll.setPrix(prix);
        return true;
    }

    public String[] getIdentifiants() {
        String[] res = new String[this.elts.size()];
        int index = 0;
        for (Collector coll : this.elts) {
            res[index++] = coll.getIdentifiant();
        }
        return res;
    }
}