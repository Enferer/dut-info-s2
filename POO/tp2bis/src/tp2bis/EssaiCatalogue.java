package tp2bis;

public class EssaiCatalogue {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Catalogue c1 = new Catalogue("Cata");
		c1.ajouter("xdf10");
		c1.ajouter("koz106", 30);
		c1.changerPrix("koz106",100);
		System.out.println(c1);
		c1.ajouter("xsf10",10);
		c1.enlever(10, "koz106");
		c1.changerPrix("xdf10", 10);
		c1.ajouter("qwerty", 3);
		c1.changerPrix("qwerty",33);
		System.out.println(c1);
		if( c1.get("azerty") == null){
			System.out.println("Azerty n'est pas Présent");
		}else{
			System.out.println("présent");
		}
		c1.enlever(5, "qwerty");
		System.out.println(c1.get("qwerty"));
		int prixTotal = 0;
		String[] tab = c1.getIdentifiants();
		for(int cpt = 0;cpt < c1.size();cpt++){
			prixTotal += c1.get(tab[cpt]).getPrix()*c1.get(tab[cpt]).getNbExemplaires();
		}
		System.out.println("Prix total : "+ prixTotal);
	}

}