package tp2bis;


public class Collector {
    private String identifiant;
    private int prix;
    private int nbExemplaires;

    Collector(String identifiant, int prix) {
        this.identifiant = identifiant;
        this.prix = prix;
        this.nbExemplaires = 0;
    }

    Collector(String identifiant) {
        this(identifiant, 0);
    }

    public int getPrix() {
        return this.prix;
    }

    public String getIdentifiant() {
        return this.identifiant;
    }

    public int getNbExemplaires() {
        return this.nbExemplaires;
    }

    void ajouter(int nbExemplaires) {
        this.nbExemplaires += nbExemplaires;
    }

    int suppression(int nbExemplaires) {
        int res = nbExemplaires;
        if (nbExemplaires <= this.nbExemplaires) {
            this.nbExemplaires -= nbExemplaires;
        } else {
            res = this.nbExemplaires;
            this.nbExemplaires = 0;
        }
        return res;
    }

    void setPrix(int prix) {
        this.prix = prix;
    }

    public String toString() {
        return String.valueOf(this.identifiant) + "(prix: " + this.prix + ", nb: " + this.nbExemplaires + ")";
    }

    public boolean equals(Object autre) {
        if (autre == null || !(autre instanceof Collector)) {
            return false;
        }
        return this.equals((Collector)autre);
    }

    public boolean equals(Collector autre) {
        return this.identifiant.equals(autre.identifiant);
    }
}