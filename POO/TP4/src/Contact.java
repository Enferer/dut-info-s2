
public class Contact {
	private String nom;
	private String email;
	
	

	
	public Contact(String nom, String email) {
		this.nom = nom;
		this.email = email;
	}
	
	public Contact(String nom) {
		this.nom = nom;
		this.email = "non renseigné";
	}

	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
