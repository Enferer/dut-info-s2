import java.time.LocalDate;

public class EssaiAgenda {
	
	public static void main(String[]args){
		Agenda a1 = new Agenda();
		LocalDate d1 = LocalDate.of(2015, 12, 31);
		LocalDate d2 = LocalDate.of(2016, 1, 1);
		Evenement e1 = new Evenement("Nouvel an","Lille",d1,d2);
		Evenement e2 = new Evenement("Tomorow land","Belgique", LocalDate.of(2016, 3, 12), LocalDate.of(2016, 3, 22));
		Evenement e3 = new Evenement("Festicale","France", LocalDate.of(2016, 3, 14), LocalDate.of(2016, 3, 27));
		//System.out.println(e1);
		a1.entrerEvenement(e3);
		a1.entrerEvenement(e2);
		a1.entrerEvenement(e1);
		System.out.println(a1);
		a1.supprimerEvenement(0);
		//System.out.println(a1);
		
		
	}
	
}
