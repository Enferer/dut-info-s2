import java.time.LocalDate;
import java.util.ArrayList;

public class Agenda {
	private ArrayList<Evenement> l1 = new ArrayList<Evenement>();

	
	public ArrayList<Evenement> getL1() {
		return l1;
	}
	
	public ArrayList<Contact> getContacts(Evenement e1){
		return l1.get(l1.indexOf(e1)).getC();
	}
	
	public ArrayList<Contact> getContacts(LocalDate d1,LocalDate d2){
		ArrayList<Contact>res = new ArrayList<Contact>();
		for (Evenement e : l1) {
			if(e.getDebut().isAfter(d1) && e.getDebut().isBefore(d2) ){
				res.addAll(e.getC());
			}
		}
		return res;
	}
	public ArrayList<Evenement> getEvenementOfContact(Contact c1){
		ArrayList<Evenement> res = new ArrayList<Evenement>();
		for (Evenement e : l1) {
			for ( Contact c2 : e.getC()) {
				if(c2.equals(c1)){
					res.add(e);
				}
			}
		}
		return res;
	}
	
	


	public String toString() {
		String res = "";
		for (int i = 0; i < l1.size(); i++) {
			res += l1.get(i).toString();
			res += '\n';
		}
		return res;
	}
	
	boolean entrable(Evenement e){
		for (int i = 0; i < l1.size(); i++) {
			if(l1.get(i).chevauche(e)){
				return false;
			}
		}
		return true;
	}
	
	void entrerEvenement (Evenement e){
		l1.add(e);
		trier();
	}
	void supprimerEvenement (Evenement e){
		l1.remove(e);
		trier();
	}
	void supprimerEvenement (int indice){
		l1.remove(indice);
		trier();
	}
	void entrerEvenement (Evenement evenement, int indice){
		l1.add(indice, evenement);
		trier();
	}
	
	void supprimerChevauchants(Evenement e){
		for (int i = 0; i < l1.size(); i++) {
			if(l1.get(i).chevauche(e)){
				l1.remove(i);
			}
		}
		trier();
	}
	void supprimerEntre(Evenement e1, Evenement e2){
		for (int i = 0; i < l1.size(); i++) {
			if(l1.get(i)!= e1 && l1.get(i)!= e2){
				if(l1.get(i).chevauche(e1) || l1.get(i).chevauche(e2)){
					l1.remove(i);
				}
			}
		}
		trier();
	}
	
	void trier(){
		boolean permut = true;
		while(permut){
			permut = false;
			for (int i = 0; i < l1.size()-1; i++) {
				if(l1.get(i).getDebut().isAfter(l1.get(i+1).getDebut())){
					Evenement e = l1.get(i);
					l1.remove(i);
					l1.add(i+1, e);
					permut = true;
				}
			}
		}
	}
}
