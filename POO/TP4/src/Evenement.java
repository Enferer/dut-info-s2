import java.time.LocalDate;
import java.util.ArrayList;

public class Evenement {
	private String intitule;
	private String lieu;
	private LocalDate debut;
	private LocalDate fin;
	private ArrayList<Contact> c;
	
	
	
	public Evenement(){
		intitule = "Non renseigné";
		lieu = "Non renseigné";
		debut = null;
		fin = null;	
		c = null;
		
	}
	
	public ArrayList<Contact> getC() {
		return c;
	}

	public void setC(ArrayList<Contact> c) {
		this.c = c;
	}

	void ajouterContact(Contact c1){
		c.add(c1);
	}
	void retirerContact(Contact c1){
		c.remove(c1);
	}
	
	
	public String toString() {
		return "Evenement [intitule=" + intitule + ", lieu=" + lieu + ", debut=" + debut + ", fin=" + fin + "]";
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Evenement other = (Evenement) obj;
		if (debut == null) {
			if (other.debut != null)
				return false;
		} else if (!debut.equals(other.debut))
			return false;
		if (fin == null) {
			if (other.fin != null)
				return false;
		} else if (!fin.equals(other.fin))
			return false;
		if (intitule == null) {
			if (other.intitule != null)
				return false;
		} else if (!intitule.equals(other.intitule))
			return false;
		if (lieu == null) {
			if (other.lieu != null)
				return false;
		} else if (!lieu.equals(other.lieu))
			return false;
		return true;
	}


	public Evenement(String intilule, String lieu, LocalDate debut, LocalDate fin) {
		this.intitule = intilule;
		this.lieu = lieu;
		if(debut.isBefore(fin)){
			this.debut = fin;
			this.fin = debut;
		}
		else{
			this.debut = debut;
			this.fin = fin;
		}
	}
	
	public String getIntilule() {
		return intitule;
	}
	public void setIntilule(String intilule) {
		this.intitule = intilule;
	}
	public String getLieu() {
		return lieu;
	}
	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	public LocalDate getDebut() {
		return debut;
	}
	/*public void setDebut(LocalDate debut) {
		this.debut = debut;
	}*/
	public LocalDate getFin() {
		return fin;
	}
	/*public void setFin(LocalDate fin) {
		this.fin = fin;
	}*/
	
	boolean chevauche(Evenement e1){
		if(fin.isAfter(e1.debut)){
			return true;
		}
		return false;
	}
	
	

}
