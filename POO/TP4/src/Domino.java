
public class Domino {
	private int f1;
	private int f2;
	
	
	public Domino(int f1,int f2){
		this.f1 = f1;
		this.f2 = f2;
	}
	public boolean equals(Domino d1){
		if(d1.getF1()==this.f1 && d1.getF2()==this.f2){
			return true;
		}
		return false;
	}
	
	public int getF1() {
		return f1;
	}
	public void setF1(int f1) {
		this.f1 = f1;
	}
	public int getF2() {
		return f2;
	}
	public void setF2(int f2) {
		this.f2 = f2;
	}
	
	
}
