import java.util.ArrayList;

public class JeuDeDomino {
	ArrayList<Domino> l1 = new ArrayList<Domino>();
	
	
	boolean add(int f1,int f2){
		Domino d = new Domino(f1,f2);
		for (int i = 0; i < l1.size(); i++) {
			if(d.equals(l1.get(i))){
				return false;
			}
		}
		l1.add(new Domino(f1,f2));
		return true;
	}
	
	public String toString(){
		String res = "";
		for (int i = 0; i < l1.size(); i++) {
			res += l1.get(i).getF1() + "|" + l1.get(i).getF2() + " , ";
		}
		return res;
	}
	
	boolean remove(int f1, int f2){
		boolean present = false;
		Domino d = new Domino(f1,f2);
		for (int i = 0; i < l1.size(); i++) {
			if(d.equals(l1.get(i))){
				present = true;
			}
		}
		if(!present){
			return false;
		}
		l1.remove(new Domino(f1,f2));
		return true;
	}
	
	void fusion(JeuDeDomino j1){
		for (int i = 0; i < j1.l1.size(); i++) {
			if(!present(j1.l1.get(i))){
				l1.add(j1.l1.get(i));
			}
		}
	}
	
	boolean present(Domino d1){
		for (int i = 0; i < l1.size(); i++) {
			if(d1.equals(l1.get(i))){
				return true;
			}
		}
		return false;
	}
}
