package tp2;

public class Inscrit {
	String dossard;
	int score;
	int temps;
	
	public Inscrit(int dossard,int score, int min, int sec){
		
		if(dossard>=0 && dossard<=100 && score >= 0 && score <=50 && min >=0 && min <= 60 ){
			this.dossard = "N°"+String.valueOf(dossard);
			this.temps = min * 60 + sec;
			this.score = score;		
		}
	}
	public String toString(){
		return "["+ dossard + ","+this.score+" point(s)"+","+this.temps+"s]";
	}
}
