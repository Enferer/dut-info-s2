package tp2;

import java.awt.Component;
//import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Tableau
extends JFrame {
    private static final long serialVersionUID = 1;

    public Tableau(int left, int top, int[][] donnees) {
        int largeur = donnees[0].length;
        int hauteur = donnees.length;
        Object[][] t = new Integer[hauteur][];
        Object[] entetes = new String[largeur];
        int j = 0;
        while (j < largeur) {
            entetes[j] = Integer.toString(j);
            ++j;
        }
        int i = 0;
        while (i < t.length) {
            t[i] = new Integer[largeur];
            int j2 = 0;
            while (j2 < largeur) {
                t[i][j2] = donnees[i][j2];
                ++j2;
            }
            ++i;
        }
        this.setTitle("Tableau");
        this.setDefaultCloseOperation(3);
        this.setLocation(left, top);
        JTable tableau = new JTable(t, entetes);
        this.getContentPane().add((Component)new JScrollPane(tableau), "Center");
        this.pack();
        this.setVisible(true);
    }

    public Tableau(int left, int top, String[][] donnees) {
        if (donnees[0][0] != null) {
            int largeur = donnees[0].length;
            Object[] entetes = new String[largeur];
            int j = 0;
            while (j < largeur) {
                entetes[j] = Integer.toString(j);
                ++j;
            }
            this.setTitle("Tableau");
            this.setDefaultCloseOperation(3);
            this.setLocation(left, top);
            JTable tableau = new JTable(donnees, entetes);
            this.getContentPane().add((Component)new JScrollPane(tableau), "Center");
            this.pack();
            this.setVisible(true);
        }
    }
}