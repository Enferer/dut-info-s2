package tp2;
import java.util.Random;

public class Grille {
	int[][]tab;
	boolean[][]tabBoolean;
	
	Grille(int x, int y){
		tab= new int[x][y];
		tabBoolean= new boolean[x][y];
	}
	boolean remplir( int min , int max, double t){
		int cpt2 = 0;
		int[] tab2 = new int[tab[0].length*tab[1].length];
		int nbralea;
		Random r = new Random();
		if(max-min < tab[0].length*tab[1].length ){
			return false;
		}else{
			for (int i = 0; i < tab.length; i++) {
				for (int j = 0; j < tab[1].length; j++) {
					nbralea = r.nextInt(max)+min;
					while(present(tab2,nbralea)){
						nbralea = r.nextInt(max)+min;
					}
					tab2[cpt2]= nbralea;
					cpt2++;
					tab[i][j]= nbralea;
				}
			}
			for (int i = 0; i < (int) tab2.length*t; i++) {
				int nbrAlea1 = r.nextInt(tab.length);
				int nbrAlea2 = r.nextInt(tab[1].length);
				tab[nbrAlea1][nbrAlea2]=0;		
			}
		}

			return true;
	}
	
	boolean present(int[]tab , int nbr){
		boolean res = false;
		for(int cpt = 0 ; cpt < tab.length ; cpt++){
			if(tab[cpt]== nbr){
				res = true;
			}
		}
		return res;
	}
	
	
	void afficher(){
		String res = "|";
		for (int i = 0; i < tab.length; i++) {
			for (int j = 0; j < tab[1].length; j++) {
				res += tab[i][j]+"|";
			}
			res+= "\n|";
		}
		System.out.println(res);
	}
	void fenetre(int left, int top){
		Tableau t = new Tableau(left,top,tab);
	}
}
