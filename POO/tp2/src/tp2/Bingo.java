package tp2;
import java.util.Random;

public class Bingo {
	Joueur[] joueur;
	
	
	Bingo(String[]tab){
		joueur = new Joueur[tab.length];
		for (int i = 0; i < tab.length; i++) {
			this.joueur[i] = new Joueur(tab[i]);
		}
	}
	Joueur Jouer(){
		Random r = new Random();
		boolean pasGagne = true;
		int nbr = r.nextInt(99);;
		while(pasGagne){
			nbr = r.nextInt(99);
			for (int cpt = 0; cpt < joueur.length; cpt++) {
				for (int j = 0; j < joueur[cpt].g.tab.length; j++) {
					for (int i = 0; i < joueur[cpt].g.tab[1].length; i++) {
						if(joueur[cpt].g.tab[j][i]== nbr){
							joueur[cpt].g.tabBoolean[j][i]=true;
						}
					}
				}
			}
			for (int i = 0; i < joueur.length; i++) {
				if(gagne(joueur[i])){
					pasGagne = false;
					return joueur[i];
				}
			}
		}
		
		Joueur j = new Joueur("ERREUR");
		return j;
	}
	
	boolean gagne(Joueur joueur){
		
		for (int j = 0; j < joueur.g.tab.length; j++) {
			for (int i = 0; i < joueur.g.tab[1].length; i++) {
				if(!(joueur.g.tabBoolean[j][i])){
					return false;
				}
			}
		}
		return true;
	}
}

