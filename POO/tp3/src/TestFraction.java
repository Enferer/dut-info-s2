import java.util.Random;

public class TestFraction {

	public static void main(String[] args) {
		/*Fraction f1 = new Fraction (1,2);
		Fraction f2 = new Fraction (3,4);
		System.out.println(f1.compareTo(f2));
		System.out.println(f1);
		System.out.println(f1.somme(f2));
		System.out.println(f1.produit(f2));
		System.out.println(f1.produit(4));*/
		
		
		Random r = new Random();
		int nbr = Integer.valueOf(args[0]);
		
		Fraction[]tab = new Fraction[nbr];
		for (int i = 0; i < tab.length; i++) {
			tab[i] = new Fraction(r.nextInt(10)+1,r.nextInt(10)+1);
		}
		
		Fraction f;
		boolean tri = true;
		while(tri){
			tri = false;
			for (int i = 0; i < tab.length-1; i++) {
				if(tab[i].compareTo(tab[i+1]) > 0){
					f = tab[i];
					tab[i] = tab[i+1];
					tab[i+1] = f;
					tri = true;
				}
			}
		}
		for (int i = 0; i < tab.length; i++) {
			System.out.println(tab[i]);
			
		}
	}
}
