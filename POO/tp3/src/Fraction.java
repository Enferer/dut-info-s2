
public class Fraction {
	double num;
	double deno;
	
	
	Fraction(){
		deno = 0;
		num = 0;
	}
	Fraction(int num){
		deno = 0;
		this.num = num;
	}
	
	Fraction(double num , double deno){
		if(deno > 0 ){
			this.num = num;
			this.deno = deno;
		}else{
			this.num = 0;
			this.deno = 0;
		}
	}
	
	public String toString(){
		if( num % deno != 0){
			return ( (int) num + "/" + (int)deno);
		}
		return (int) (num/deno)+"";
		
	}
	
	double compareTo(Fraction f){
		return (this.num/this.deno - f.num/f.deno); 
	}
	
	public Fraction somme(Fraction f){
		double numRes;
		double denoRes;
		if(this.deno == f.deno){
			numRes = this.num + f.num;
			denoRes = f.deno;
		}else{
			numRes = (this.num * f.deno)+(f.num*this.deno);
			denoRes = (this.deno * f.deno);
		}
		return new Fraction(numRes,denoRes);
	}
	
	public Fraction somme(int n){
		double numRes = this.num + n*this.deno;
		return new Fraction(numRes,this.deno);
	}
	
	public Fraction produit(Fraction f){
		double numRes = this.num*f.num;
		double denoRes = this.deno*f.deno;
		return new Fraction(numRes,denoRes);
	}
	
	public Fraction produit(int n){
		double numRes = this.num * n;
		return new Fraction(numRes,this.deno);
	}
	
}
