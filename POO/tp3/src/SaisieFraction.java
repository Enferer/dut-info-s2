import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class SaisieFraction {

	public static void main(String[] args) {
		
		int	num = controle(Integer.valueOf(args[0]),Integer.valueOf(args[1]),"Rentre le numerateur");
		int deno;
		do{
			deno = controle(Integer.valueOf(args[0]),Integer.valueOf(args[1]),"Rentre le denominateur");
		}while(deno == 0);
		Fraction f1 = new Fraction(num,deno);
		System.out.println(f1);
		
	}
	static int controle(int min , int max,String msg){
		JFrame frame = new JFrame();
		String saisie = "";
		do{
	
			saisie = JOptionPane.showInputDialog(frame,msg);

		}while(saisie.matches("[0-9][0-9]*") && Integer.valueOf(saisie) < min || Integer.valueOf(saisie) > max);
		return Integer.valueOf(saisie);
	}

}
