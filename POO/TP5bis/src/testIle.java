import java.util.Random;


public class testIle {

	public static void main(String[] args) {
		
		Random r=new Random();
		String[] gifs={"images/char1.png","images/trois.gif","images/deux.gif","images/quatre.gif"};
		int taille=13;
		Afficheur grille=new Afficheur(gifs,taille);

		int[][] jeu=new int[taille][taille];
		
		// Remplissage aléatoire du tableau
		for (int i=0;i<taille;i++){
			for (int j=0;j<taille;j++){
				if (i == 0 || j == 0 || i == taille-1 || j == taille-1 ) {
					jeu[i][j]= 3;
				}else if( i == 1 || i == taille-2 || j == 1 || j == taille-2){
					if(r.nextBoolean()){
						jeu[i][j]=3;
					}else{
						jeu[i][j]=1;
					}
				}else{
					jeu[i][j]= 1;
				}
			
			}
		}
		grille.setJeu(jeu);
		grille.affichage();
		
	}

}
