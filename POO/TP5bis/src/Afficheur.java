



import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

	

public class Afficheur
  extends JPanel
{
  private static final long serialVersionUID = 1L;
  private int nbImages;
  private ImageIcon[] images;
  private int dimImage;
  private int[][] jeu;
  
  public Afficheur(String[] gif, int largeur,int longueur)
  {
    jeu = new int[largeur][longueur];
    JFrame plateau = new JFrame();
    if (gif != null) {
      nbImages = gif.length;
      images = new ImageIcon[nbImages];
      for (int i = 0; i < nbImages; i++) images[i] = new ImageIcon(gif[i]);
      dimImage = (images[0].getIconHeight() + 2);
      plateau.setTitle("Territoire (" + largeur + "X" + longueur + ")");
      plateau.setSize(largeur * dimImage + 100, longueur * dimImage + 100);
      plateau.setLocationRelativeTo(null);
      plateau.setLayout(new FlowLayout());
      plateau.setDefaultCloseOperation(3);
      
      setPreferredSize(new Dimension(largeur * dimImage, longueur * dimImage));
      setBackground(Color.BLACK);
      plateau.getContentPane().add(this);
      plateau.setVisible(true);
    }
  }
  

  public void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    
    Dimension taille = getSize();
    int x = 2;int y = 1;int lg = 0;int col = 0;
    g.setColor(Color.white);
    while (y < getHeight()) {
      while (x < getWidth()) {
        if (jeu[col][lg] != 0) {
          g.drawImage(images[(jeu[col][lg] - 1)].getImage(), x, y, null);
        } else
          g.drawRect(x, y, dimImage - 2, dimImage - 2);
        x += dimImage;
        col++;
      }
      lg++;
      col = 0;
      x = 2;
      y += dimImage;
    }
  }
  




  public void setJeu(int[][] jeu)
  {
    this.jeu = jeu;
  }
  

  public int[][] getJeu()
  {
    return jeu;
  }
  
  public void affichage()
  {
    repaint();
  }
}