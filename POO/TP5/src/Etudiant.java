package P1;
import java.time.LocalDate;

public class Etudiant {

	private Personne p;
	private int NIP;
	private String formation;
	private double moyenne = 0;
	
	public Etudiant(String nom,String prenom, LocalDate d,String f){
		p = new Personne(nom,prenom,d);
		formation = f;
	}

	public String getNom() {
		return p.getNom();
	}

	public String getPrenom() {
		return p.getPrenom();
	}

	public int getAge() {
		return p.getAge();
	}

	public boolean plusVieux(Etudiant e) {
		return this.p.plusVieux(e.p);
	}

	public String toString() {
		return "Etudiant [p=" + p + ", NIP=" + NIP + ", formation=" + formation + ", moyenne=" + moyenne + "]";
	}

	public void setMoyenne(double moyenne) {
		this.moyenne = moyenne;
	}

	public int getNIP() {
		return NIP;
	}

	public void setNIP(int nIP) {
		NIP = nIP;
	}

	public String getFormation() {
		return formation;
	}

	public double getMoyenne() {
		return moyenne;
	}
	
	
	
	/*public static void main(String[] args) {
		// TODO Auto-generated method stub

	}*/

}
