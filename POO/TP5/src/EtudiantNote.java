package P1;
import java.time.LocalDate;
import java.util.ArrayList;

public class EtudiantNote {

	private Etudiant e;
	private ArrayList<Double> notes;
	
	public String getNom() {
		return e.getNom();
	}
	public int getAge() {
		return e.getAge();
	}
	public boolean plusVieux(EtudiantNote e2) {
		return e.plusVieux(e2.e);
	}
	public String getFormation() {
		return e.getFormation();
	}
	public double getMoyenne() {
		return e.getMoyenne();
	}
	public int getNip(){
		return e.getNIP();
	}

	public EtudiantNote(String nom,String prenom, LocalDate d,String f){
		e = new Etudiant(nom,prenom,d,f);
	}
	public void ajouter(double note){
		if(note >= 0 && note <=20){
			notes.add(note);
		}
	}
	public void ajouterNote(Double n){
		notes.add(n);
		double total = 0.0;
		for (Double note : notes) {
			total += note;
		}
		e.setMoyenne(total/(notes.size()));
	}
	
	
	
	
		
	/*	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}*/

}
