package P1;
import java.time.LocalDate;

public class Personne {

	private String nom;
	private String prenom;
	private LocalDate dateNaissance;
	
	
	
	public Personne(String nom, String prenom, LocalDate dateNaissance) {
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
	}
	
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public int getAge() {
		return LocalDate.now().getYear() - dateNaissance.getYear();
	}
	
	public boolean plusVieux(Personne p){
		return this.dateNaissance.isAfter(p.dateNaissance);
	}

	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", age =" + this.getAge() + "]";
	}
	
	
	
	
	
	/*public static void main(String[] args) {
		// TODO Auto-generated method stub

	}*/

}
