package P1;
import java.util.ArrayList;

public class Formation {

	private ArrayList<EtudiantNote> etu = new ArrayList<EtudiantNote>();
	private String formation;
	
	public Formation(String n){
		formation = n;
	}
	public double getMoy(){
		double total = 0;
		for (EtudiantNote e : etu) {
			total += e.getMoyenne();
		}
		return total/etu.size();
	}
	public void exclure(int nip){
		for (int i = 0; i < etu.size(); i++) {
			if (etu.get(i).getNip() == nip) {
				etu.remove(i);
			}		
		}
	}
	
	public void ajouter(EtudiantNote e){
		etu.add(e);
	}
	
	public String getFormation(){
		return formation;
	}
	
}
