//import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
//import java.time.format.FormatStyle;

public class EssaiDateTimeFormatter {

	public static void main(String[] args) {
		LocalDateTime date = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		String text = date.format(formatter);
		System.out.println(text);
		formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		text = date.format(formatter);
		System.out.println(text);
		formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy h:m:s");
		text = date.format(formatter);
		System.out.println(text);
		formatter = DateTimeFormatter.ofPattern("dd L yyyy");
		text = date.format(formatter);
		System.out.println(text);
		
		
		System.out.println(date.format(DateTimeFormatter.ISO_LOCAL_TIME));
		
	}

}