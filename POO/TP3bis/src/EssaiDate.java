import java.time.LocalDate;
import java.time.Period;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class EssaiDate {

	static LocalDate Date(){
		int annee = controle(1,LocalDate.MAX.getYear(),"Choisi une annee");
		int mois = controle(1,12,"Choisi un moi");
		int nbrJour = 31;
		if((mois%2)==0){
			nbrJour = 30;
		}
		int jour = controle(1,nbrJour,"Choisi un jour");
		
		return LocalDate.of(annee,mois,jour);
	}
	
	static int controle(int min , int max,String s){
		JFrame frame = new JFrame();
		int saisie = -1;
		do{
	
			saisie = Integer.valueOf(JOptionPane.showInputDialog(frame,s));
		}while(saisie < min || saisie > max);
		return saisie;
	}
	
	
	public static void main(String[] args) {
		//Random r = new Random();
		JFrame f = new JFrame();
		//JOptionPane.showMessageDialog(f,LocalDate.now());
		
		
		//******* TEST ECART ENTRE DEUX DATES *******
		LocalDate d1 = Date();
		JOptionPane.showMessageDialog(f,d1);
		LocalDate d2 = Date();
		JOptionPane.showMessageDialog(f,d2);
		Period p  = d1.until(d2);
		int ecart = p.getYears()*365;
		ecart += p.getMonths()*31;
		ecart += p.getDays();
		JOptionPane.showMessageDialog(f,"Il y "+ecart+" jour(s) d'écart entre les deux dates");
		//****** FIN ECART ******

		//****** Début date aléatoire trié *******
		
		/*LocalDate[] tab = new LocalDate[3];
		int mois;
		int nbrJour;
		for (int i = 0; i < tab.length; i++) {
			mois = r.nextInt(12)+1;
			nbrJour = 31;
			if((mois%2)==0){
				nbrJour = 30;
			}
			tab[i] = LocalDate.of(r.nextInt(20)+2000,mois,r.nextInt(nbrJour)+1);
		}
		boolean tri = true;
		LocalDate d;
		while(tri){
			tri = false;
			for (int i = 0; i < tab.length-1; i++) {
				if(tab[i].isAfter(tab[i+1])){
					d = tab[i];
					tab[i] = tab[i+1];
					tab[i+1] = d;
					tri = true;
				}
			}
		}
		for (int i = 0; i < tab.length; i++) {
			System.out.println(tab[i]);
		}*/
		// ******** fin date alea trié ****
		
	}

}