
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Saisie {

	static int controle(int min , int max){
		JFrame frame = new JFrame();
		int saisie = -1;
		do{
	
			saisie = Integer.valueOf(JOptionPane.showInputDialog(frame,"Entre un nombre entre "+min+" et "+max));

		}while(saisie < min || saisie > max );
		return saisie;
	}
	
	public static void main(String[] args) {
		JFrame j = new JFrame();
		int n = controle(10,100);
		JOptionPane.showMessageDialog(j,"Vous avez choisi "+n);
	/*	JFrame frame = new JFrame();
		String s = JOptionPane.showInputDialog(frame,"Comment t'apelle-tu");
		System.out.println(s);*/
		
		//JOptionPane.showMessageDialog(frame, "Salut");
		
	}

}