import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
//import java.awt.Container;
import java.awt.Dimension;
//import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.Window;
import java.util.Map;
//import java.util.Set;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.Plot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.RefineryUtilities;

public class Camembert
extends JPanel {
    private static final long serialVersionUID = 1;
    private DefaultPieDataset donnees = new DefaultPieDataset();
    private JFreeChart camembert;

    public Camembert(String titre, Map<String, Float> valeurs, Map<String, Color> couleurs) {
        if (valeurs != null) {
            JFrame fenetre = new JFrame("Diagramme circulaire");
            fenetre.getContentPane().setLayout(new BorderLayout());
            fenetre.getContentPane().setPreferredSize(new Dimension(700, 500));
            fenetre.setDefaultCloseOperation(3);
            fenetre.pack();
            RefineryUtilities.centerFrameOnScreen((Window)fenetre);
            fenetre.setVisible(true);
            fenetre.getContentPane().add(this);
            if (this.renseigner(valeurs)) {
                this.camembert = ChartFactory.createPieChart3D((String)titre, (PieDataset)this.donnees, (boolean)true, (boolean)true, (boolean)false);
                if (this.colorer(couleurs)) {
                    this.revalidate();
                }
            }
        }
    }

    public boolean renseigner(Map<String, Float> valeurs) {
        if (valeurs == null) {
            return false;
        }
        for (String cle : valeurs.keySet()) {
            this.donnees.setValue((Comparable)((Object)cle), (Number)valeurs.get(cle));
        }
        return true;
    }

    public boolean colorer(Map<String, Color> couleurs) {
        ChartPanel portions = new ChartPanel(this.camembert);
        Plot plot = this.camembert.getPlot();
        for (String cle : couleurs.keySet()) {
            ((PiePlot)plot).setSectionPaint((Comparable)((Object)cle), (Paint)couleurs.get(cle));
        }
        this.add((Component)portions);
        return true;
    }
}