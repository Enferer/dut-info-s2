import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SuperCamembert {
	
	private Camembert b;
	
	public SuperCamembert(String titre, Map<String, Float> valeurs, Map<String, Color> couleurs) {
		b = new Camembert(titre, valeurs, couleurs);
	}
	public SuperCamembert(String titre, String[]legende,float[]valeurs,Color[]couleurs){
		HashMap<String, Float> m = new HashMap<String, Float>();
		HashMap<String, Color> m2 = new HashMap<String, Color>();
		for (int i = 0; i < legende.length; i++) {
			m.put(legende[i], valeurs[i]);
			m2.put(legende[i], couleurs[i]);
		}
		b = new Camembert(titre,m,m2);
	}
	boolean renseigner(String[] legende,float[] valeurs){
		HashMap<String, Float> m = new HashMap<String, Float>();
		for (int i = 0; i < legende.length; i++) {
			m.put(legende[i], valeurs[i]);
		}
		return b.renseigner(m);
	}
	boolean colorer(String[] legende,Color[] couleurs){
		HashMap<String, Color> m = new HashMap<String, Color>();
		for (int i = 0; i < legende.length; i++) {
			m.put(legende[i], couleurs[i]);
		}
		return b.colorer(m);
	}
	SuperCamembert(String titre,HashMap<String,Float> valeurs){
		HashMap<String,Color> couleur = new HashMap<String,Color>();
		Set<String> set = valeurs.keySet();
		Object[] tab = set.toArray();
		Color c = Color.RED;
		c = c.brighter();
		for (int i = 0; i < tab.length; i++) {
			couleur.put(String.valueOf(tab[i]),c);
			c = c.darker();
		}
		b = new Camembert(titre,valeurs,couleur);
		

	}
}
