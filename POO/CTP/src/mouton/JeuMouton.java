package mouton;

import java.util.ArrayList;


public class JeuMouton implements Jeu{
	
	private final int SIZE;
	protected int idxVide = 3;
	private Case[] plateau;

	
	public JeuMouton(int size){
		if(size%2 == 1 && size>0){
			this.SIZE = size;
		}else{
			this.SIZE = 9;
		}
		plateau = new Case[this.SIZE];
		for (int i = 0; i < this.SIZE; i++) {
			if(i < (int)this.SIZE/2){
				plateau[i] = Case.DROIT;
			}else if( i == ((int)this.SIZE/2)){
				plateau[i] = Case.VIDE;
			}else{
				plateau[i] = Case.GAUCHE;
			}
		}
		
	}
	
	@Override
	public ArrayList<Integer> coupsPossibles() {
		ArrayList<Integer> res = new ArrayList<Integer>(); 
		for (int i = 0; i < plateau.length; i++) {
			if(ok(i)){
				res.add(i);
			}
		}
		return res;
	}

	@Override
	public void executer(int coup) {
		if(ok(coup)){
			if(this.plateau[coup].toString().equals(">")){
				if (this.plateau[coup+1].toString().equals(" ")) {
					plateau[coup] = Case.VIDE;
					plateau[coup+1] = Case.DROIT;
				}else{
					plateau[coup] = Case.VIDE;
					plateau[coup+2] = Case.DROIT;
				}
			}else{
				if (this.plateau[coup-1].toString().equals(" ")) {
					plateau[coup] = Case.VIDE;
					plateau[coup-1] = Case.GAUCHE;
				}else{
					plateau[coup] = Case.VIDE;
					plateau[coup-2] = Case.GAUCHE;
				}
			}
		}
	}
	protected boolean ok(int c){
		if(c < 0 || c >= this.SIZE){
			return false;
		}
		if(this.plateau[c].toString().equals(" ")){
			return false;
		}
		// MOUTON DROIT
		if(this.plateau[c].toString().equals(">")){
			if(c+1 >= this.SIZE){
				return false;
			}else if(this.plateau[c+1].toString().equals(" ")){
				return true;
			}else if(c+2 >= this.SIZE){
				return false;
			}else if(this.plateau[c+2].toString().equals(" ")){
				return true;
			}else{
				return false;
			}
		}
		// FIN MOUTON DROIT
		// MOUTON GAUCHE
		else if(this.plateau[c].toString().equals("<")){
			if(c-1 < 0){
				return false;
			}else if(this.plateau[c-1].toString().equals(" ")){
				return true;
			}else if(c-2 < 0){
				return false;
			}else if(this.plateau[c-2].toString().equals(" ")){
				return true;
			}else{
				return false;
			}
		}
		// FIN MOUTON GAUCHE
		else{
			return false;
		}
	}

	@Override
	public boolean gagnant() {
		boolean res = false;
		for (int i = 0; i < this.SIZE; i++) {
			if(i < (int)this.SIZE/2){
				res = plateau[i].equals(Case.GAUCHE);
			}else if( i == ((int)this.SIZE/2)){
				res = plateau[i].equals(Case.VIDE);
			}else{
				res = plateau[i].equals(Case.DROIT);
			}
		}
		return res;
	}

	@Override
	public Case getCase(int i) {
		return plateau[i];
	}

	@Override
	public boolean perdant() {
		return coupsPossibles().isEmpty();
	}

	public int getSIZE() {
		return SIZE;
	}

	public int getIdxVide() {
		return idxVide;
	}
	
	

}
