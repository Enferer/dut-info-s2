package mouton;

import java.util.ArrayList;
import java.util.Scanner;

public class JoueurHumain implements Joueur{


	public int coupAJouer(ArrayList<Integer> possibles) {
		System.out.println("Saisissez un nombre");
		int res = -1;
		Scanner s = new Scanner(System.in);
		boolean saisieCorrect = false;
		do{
			String saisie = s.nextLine();
			try {
				res = Integer.valueOf(saisie);
				saisieCorrect = true;
			} catch (Exception e) {
				System.out.println("Veulliez rentrer un nombre");
			}
		}while(!saisieCorrect);
		
		return res;
		
	}
	

}
