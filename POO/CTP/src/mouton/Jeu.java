package mouton;

import java.util.ArrayList;

public interface Jeu {

	
	ArrayList<Integer> coupsPossibles();
	void executer(int coup);
	boolean gagnant();
 	Case getCase(int i);
 	boolean perdant();
}
