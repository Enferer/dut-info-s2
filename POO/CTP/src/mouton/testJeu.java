package mouton;

public class testJeu {

	Jeu jeu;
	Joueur joueur;
	
	public testJeu(int size){
		jeu = new JeuMouton(3);
		joueur = new JoueurHumain();
		do{
			affichageJeu(jeu);
			jeu.executer(joueur.coupAJouer(jeu.coupsPossibles()));
			
		}while (!jeu.perdant() && !jeu.gagnant());
		if(jeu.gagnant()){
			System.out.println("Vous avez gagné");
		}else{
			System.out.println("Vous avez perdu");
		}
	}
	
	
	public static void main(String[] args) {
		testJeu t = new testJeu(9);
	}
	
	private void affichageJeu(Jeu j){
		for (int i = 0; i < ((JeuMouton)j).getSIZE(); i++) {
			System.out.print(j.getCase(i));
		}
		System.out.println("");
	}

}
