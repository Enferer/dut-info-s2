package mouton;

public enum Case {

	DROIT('>'),
	GAUCHE('<'),
	VIDE(' ');
	
		
	char representation;
	
	Case(char rep){
		this.representation = rep;
	}
	
	public String toString(){
		return representation+"";
	}

}
