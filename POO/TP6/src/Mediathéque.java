import java.time.LocalDate;
import java.util.ArrayList;

public class Mediathéque {
	
	
	private ArrayList<Document> l1 = new ArrayList<Document>();
	
	
	public void ajouterDocument(Document d){
		l1.add(d);
	}
	public Document rechercherCote(String cote){
		for (int i = 0; i < l1.size(); i++) {
			if (l1.get(i).getCote() == cote) {
				return l1.get(i);
			}
		}
		return null;
	}
	
	private int rechercherIdxCote(String cote){
		for (int i = 0; i < l1.size(); i++) {
			if (l1.get(i).getCote() == cote) {
				return i;
			}
		}
		return -1;
	}
	
	public boolean emprunter(String cote,int emprunteur){
		int idx = rechercherIdxCote(cote);
		return l1.get(idx).emprunt(emprunteur,LocalDate.now());
	}
	public boolean emprunter(String cote,int emprunteur,LocalDate d){
		int idx = rechercherIdxCote(cote);
		return l1.get(idx).emprunt(emprunteur,d);
	}
	
	
	public boolean restituer(String cote){
		int idx = rechercherIdxCote(cote);
		return l1.get(idx).restitution();
	}
	public String emprunts(){
		String res = "Document emprunté : \n";
		for (Document document : l1) {
			if (!document.estLibre()) {
				res += document.toString()+"\n";
			}
		}
		return res;
	}
	public String enRetard(){
		String res = "";
		for (Document document : l1) {
			if(document.estEnRetard()){
				res += document.toString()+"\n";
				
			}
		}
		return res;
	}
	
	public String toString() {
		return "Mediathéque [ l1= " + l1 + " ]";
	}
	
	
	public static void main(String[] args) {
		Mediathéque m = new Mediathéque();
		m.ajouterDocument(new Dvd("Harry potter 1",LocalDate.of(2000, 11, 11),"Harry_Potter_1_film","David Yates",120));
		m.ajouterDocument(new Dvd("Harry potter 2",LocalDate.of(2001, 11, 11),"Harry_Potter_2_film","David Yates",135));
		m.ajouterDocument(new Dvd("Harry potter 3",LocalDate.of(2002, 11, 11),"Harry_Potter_3_film","David Yates",140));
		m.ajouterDocument(new Dvd("Harry potter 1",LocalDate.of(2000, 11, 11),"Harry_Potter_1","JK rowling",520));
		m.ajouterDocument(new Dvd("Harry potter 2",LocalDate.of(2001, 11, 11),"Harry_Potter_2","JK rowling",735));
		m.ajouterDocument(new Dvd("Harry potter 3",LocalDate.of(2002, 11, 11),"Harry_Potter_3","JK rowling",440));
		
		System.out.println(m.rechercherCote("Harry_Potter_2"));
		System.out.println(m);
		System.out.println("-------------------Film emprunter----------------------");
		m.emprunter("Harry_Potter_2", 42);
		m.emprunter("Harry_Potter_1_film", 8 , LocalDate.of(1998,11,14));
		m.emprunter("Harry_Potter_2_film", 8 , LocalDate.of(2017,3,8));
		m.emprunter("Harry_Potter_3_film", 8 , LocalDate.of(2017,2,20));
		System.out.print(m.emprunts());
		System.out.println("-------------------En retard-------------------");
		System.out.println(m.enRetard());
		
		
	}
}
