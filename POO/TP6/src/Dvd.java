import java.time.LocalDate;

public class Dvd extends Document{
	
	private String producteur;
	private int duree;
	
	
	
	public Dvd(String titre, LocalDate date, String cote, String producteur, int duree) {
		super(titre, date, cote);
		this.producteur = producteur;
		this.duree = duree;
	}
	
	public String getProducteur() {
		return producteur;
	}
	public void setProducteur(String producteur) {
		this.producteur = producteur;
	}
	public int getDuree() {
		return duree;
	}
	public void setDuree(int duree) {
		this.duree = duree;
	}
	
	
}
