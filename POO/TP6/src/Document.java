import java.time.LocalDate;
import java.time.Period;

public class Document {

	private String titre;
	private LocalDate date;
	private String cote;
	private boolean libre = true;
	private int emprunteur = 0;
	LocalDate dateEmprunt;
	
	
	public Document(String titre,LocalDate date, String cote) {
		this.titre = titre;
		this.date = date;
		this.cote = cote;
	}
	
	public int dureeEmprunt(){
		if(this instanceof Dvd){
			return 5;
		}
		return 15;
	}
	
	public boolean aPourCote(String cote){
		return this.cote == cote;
	}
	
	public boolean emprunt(int emprunteur,LocalDate d1){
		if (libre) {
			this.emprunteur = emprunteur;
			libre = false;
			this.dateEmprunt = d1;
			return true;
		}
		return false;
	}
	
	public boolean estEnRetard(){
		if(dateEmprunt != null){
			Period p = LocalDate.now().until(dateEmprunt);
			if(p.getDays()>-dureeEmprunt()){
				return true;
			}
		}
		return false;
	}
	
	public boolean restitution(){
		if (libre) {
			return false;
		}
		this.dateEmprunt = null;
		emprunteur = 0;
		libre = true;
		return true;
	}
	public boolean estLibre(){
		return libre;
	}
	
	public String toString() {
		return "Document [ titre= " + titre + ", date= " + date + ", cote= " + cote + " ]   ";
	}
	
	public boolean equals(Document d){
		return toString().equals(d.toString());
	}
	
	public String getTitre() {
		return titre;
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	public LocalDate getDate() {
		return date;
	}
	
	public void setDate(LocalDate date) {
		this.date = date;
	}
	
	public String getCote() {
		return cote;
	}
	
	public void setCote(String cote) {
		this.cote = cote;
	}



	
	
	

	/*public static void main(String[] args) {
		// TODO Auto-generated method stub

	}*/

}
