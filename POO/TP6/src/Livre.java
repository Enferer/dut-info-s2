import java.time.LocalDate;

public class Livre extends Document{
	String editeur;
	int pages;
	
	
	
	
	public Livre(String titre, LocalDate date, String cote, String editeur, int pages) {
		super(titre, date, cote);
		this.editeur = editeur;
		this.pages = pages;
	}
	public String getEditeur() {
		return editeur;
	}
	public void setEditeur(String editeur) {
		this.editeur = editeur;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	
	
}
