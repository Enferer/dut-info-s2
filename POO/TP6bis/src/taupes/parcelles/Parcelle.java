package taupes.parcelles;

public class Parcelle{
	Taupe t;
	Coordonnees c;
	private int trou = 0;
	private boolean tas = false;
	private boolean mur = false;
	
	public Parcelle(int x, int y){
		c = new Coordonnees(x,y);
	}
	public Coordonnees getCoordonnees(){
		return new Coordonnees(c.getX(),c.getY());
	}
	public void setCoordonnees(Coordonnees c){
		c.modifier(c);
	}
	public Taupe getTaupe(){
		return t;
	}
	public int estTrou(){
		return trou;
	}
	public void setTrou(int x){
		trou = x;
	}
	public boolean estTas(){
		return tas;
	}
	public void setTas(boolean tas) {
		this.tas = tas;
	}
	public void setMur() {
		this.mur = true;
	}
	public boolean estMur(){
		return mur;
	}
	public int estTaupe(){
		if (t == null) {
			return 0;
		}
		return t.getEquipe(); 
	}
	public void poserTas(){
		tas = true;
	}
	public void retirerTas(){
		tas = false;
	}
	public void boucherTroue(){
		trou = 0;
	}
	public void poserTaupe(Taupe t){
		this.t = t;
	}
	public void retirerTaupe(){
		t = null;
	}
	public void creserTrou(){
		trou = t.getEquipe();
	}
	public void vider(){
		t = null;
		trou = 0;
		tas = false;
		mur = false;
	}
	
	
}
