package taupes.parcelles;

public class Coordonnees {
	int x;
	int y;
	
	//final static Constante constante = new Constante();
	
	public Coordonnees(int x,int y){
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public void modifier(Coordonnees c){
		this.x = c.x;
		this.y = c.y;
	}
	
}
