package taupes.parcelles;

public class Taupe {
	private int equipe = 1;
	Vue v;
	Coordonnees c;
	
	public Taupe(int equipe, Vue v) {
		this.equipe = equipe;
		this.v = v;
	}
	public Taupe(int equipe,Vue v, int x, int y){
		this.equipe = equipe;
		this.v = v;
		c = new Coordonnees(x,y);
	}
	public int getEquipe(){
		return equipe;
	}
	public Coordonnees getCoordonnes() {
		return c;
	}
	public void setCoordonnes(Coordonnees c) {
		this.c = c;
	}
	public Vue getV() {
		return v;
	}
	
	
	
	
	
	
}
