package taupes.parcelles;

import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
//import javax.swing.JOptionPane;
import javax.swing.JPanel;

import taupes.parcelles.Parcelle;
import taupes.parcelles.Taupe;

	/**
	 * La classe Terrain représente une grille carrée de parcelles sur laquelle évoluent les taupes
	 */
public class Terrain extends JPanel{
	/**
	 *   Grille de parcelles
	 */
	private static Parcelle[][] terrain; 
	private static Map<String,ImageIcon> images= new HashMap<>();

	/**
	 * taille d'une parcelle
	 */
	private static final int tailleParcelle=new ImageIcon("images/terre.jpg").getIconHeight()+2;

	/**
	 *  taille du territoire
	 */
	private static int tailleTerrain; 
		
	/**
	 * Retourne la taille du terrain
	 * @return la taille du terrain
	 */
	public static int getTaille(){return tailleTerrain;}
	
	/**
	 * Indicateur de fin de partie
	 */
	public static boolean fin = false;

	/**
	 * Crée une fenêtre et affiche le territoire dedans
	 * @param taille la taille du territoire
	 */
	public Terrain(int taille) {
		Terrain.tailleTerrain=taille;
		terrain=new Parcelle[tailleTerrain][tailleTerrain];
		
		// Les images manipulées
		images.put("terre",new ImageIcon("images/terre.jpg"));
		images.put("mur",new ImageIcon("images/mur.jpg"));
		images.put("tas",new ImageIcon("images/tas.jpg"));
		images.put("trou",new ImageIcon("images/trou.jpg"));
		images.put("taupe1",new ImageIcon("images/taupe1.jpg"));
		images.put("taupe2",new ImageIcon("images/taupe2.jpg"));
		// Taille de la fenêtre
		Frame f = new Frame("Terrain");
		f.setBounds(0,100, tailleParcelle*tailleTerrain+20, tailleParcelle*tailleTerrain+50);
		f.add(this);
		// Remplissage du terrain
		for (int i=0; i<tailleTerrain-1;i++) {
			// Pose des murs
			this.setMur(i,0);
			this.setMur(0,i);
			this.setMur(i,tailleTerrain-1);
			this.setMur(tailleTerrain-1,i);
			// parcelles de terrain
			for (int j=1;j<tailleTerrain-1;j++) terrain[i][j]=new Parcelle(i,j);
		}
		this.setMur(tailleTerrain-1,tailleTerrain-1);
		
		// Fermeture de la fenêtre
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e)  {System.exit(0);}
		});
		// Affichage de la fenêtre
		f.setVisible(true);
	}

	/**
	 * (non-Javadoc)
	 * @see javax.swing.JComponent#update(java.awt.Graphics)
	 */
	public void update(Graphics g) { paint(g); }
	
	/**
	 *(non-Javadoc)
	 * @see javax.swing.JComponent#paint(java.awt.Graphics)
	 */
	public void paint(Graphics g) {
		for (int x=0; x<tailleTerrain; x++){
			for (int y=0; y<tailleTerrain; y++){ 
				if (estMur(x,y)) {
					g.drawImage(images.get("mur").getImage(),x*tailleParcelle,y*tailleParcelle,null);
				}
				else {
					if(estTrou(x,y) != 0){
						g.drawImage(images.get("trou").getImage(),x*tailleParcelle,y*tailleParcelle,null);
					}
					else if (estTas(x,y)) {
						g.drawImage(images.get("tas").getImage(),x*tailleParcelle,y*tailleParcelle,null);
					}
					
					else{ 
						switch (estTaupe(x,y))
						{
						case 0:{
							g.drawImage(images.get("terre").getImage(),x*tailleParcelle,y*tailleParcelle,null);
							break;
						}
						case 1: {
							g.drawImage(images.get("taupe1").getImage(),x*tailleParcelle,y*tailleParcelle,null);
							break;
						}
						case 2: {
							g.drawImage(images.get("taupe2").getImage(),x*tailleParcelle,y*tailleParcelle,null);
							break;
						}

						}
					}
				}
			}
		}
	}
	
	public boolean estMur(int x, int y){
		return terrain[x][y].estMur();
	}
	public boolean estTas(int x, int y){
		return terrain[x][y].estTas();
	}
	public int estTrou(int x, int y){
		return terrain[x][y].estTrou();
	}
	public int estTaupe(int x, int y){
		return terrain[x][y].estTaupe();
	}
	
	public void poserTas(int x,int y){
		terrain[x][y].poserTas();
	}
	public void creserTrou(int x,int y){
		terrain[x][y].creserTrou();
	}
	public void poserTaupe(int x,int y, Taupe t){
		terrain[x][y].poserTaupe(t);
	}
	public void setLibre(int x , int y){
		terrain[x][y].vider();
	}
	public void setMur(int x,int y){
		terrain[x][y].setMur();
	}
	
}




