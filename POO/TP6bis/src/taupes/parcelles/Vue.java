package taupes.parcelles;

public class Vue {
	private Terrain terrain;
	
	public Vue(Terrain t){
		terrain = t;
	}
	public void poserTaupe(Taupe t , Coordonnees c){
		terrain.poserTaupe(c.getX(),c.getY(), t);
	}
	public boolean estTerrain(Coordonnees c){
		return !(terrain.estMur(c.getX(), c.getY()));
	}
	
	public void setLibre(Coordonnees c){
		terrain.setLibre(c.getX(), c.getY());
	}
	public boolean estTas(Coordonnees c){
		return terrain.estTas(c.getX(),c.getY());
	}
	public void poserTas(Coordonnees c){
		terrain.poserTas(c.getX(),c.getY());
	}
	public void creserTrou(Taupe t , Coordonnees c){
		terrain.creserTrou(c.getX(), c.getY());
	}
	
}
