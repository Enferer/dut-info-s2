
import java.util.ArrayList;

import com.sun.javafx.scene.paint.GradientUtils.Point;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.effect.PerspectiveTransform;
import javafx.scene.image.Image;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class HelloPerspective extends Application {
	
	private final int largeurMenuDeGauche = 200;
	ArrayList<Point2D> listeDePoint = new ArrayList<Point2D>();

  public void start(Stage stage) {
    HBox root = new HBox();
    


    
    Canvas canvas = new Canvas (500, 504);
    GraphicsContext gc = canvas.getGraphicsContext2D();

    Image imageReelle = new Image("imageReelle.png");
    Image imageVirtuelle = new Image("imageVirtuelle.png");



    gc.drawImage(imageReelle, 0, 0);
    gc.setEffect(null);
  
    root.setOnMouseClicked(e->{
    	if (listeDePoint.size()<=4) {
    		listeDePoint.add(new Point2D(e.getX(), e.getY()));
		}
       	if(listeDePoint.size()==4){
    		PerspectiveTransform pt = new PerspectiveTransform();
    		pt.setUlx(listeDePoint.get(0).getX()-largeurMenuDeGauche);
    	    pt.setUly(listeDePoint.get(0).getY());
    	    pt.setUrx(listeDePoint.get(1).getX()-largeurMenuDeGauche);
    	    pt.setUry(listeDePoint.get(1).getY());
    	    pt.setLrx(listeDePoint.get(2).getX()-largeurMenuDeGauche);
    	    pt.setLry(listeDePoint.get(2).getY());
    	    pt.setLlx(listeDePoint.get(3).getX()-largeurMenuDeGauche);
    	    pt.setLly(listeDePoint.get(3).getY());

    	    gc.drawImage(imageReelle, 0, 0);
    	    gc.setEffect(pt);
    	    gc.drawImage(imageVirtuelle, 0, 0);
     	 //   repaint();
    	}
    });
    
   // Initilisation menu de gauche
    
    VBox menuGauche = new VBox();
    menuGauche.setMinWidth(this.largeurMenuDeGauche);
    menuGauche.setBorder(new Border(new BorderStroke(Color.BLACK,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,new BorderWidths(2),new Insets(0))));
    //--------- titre --------------
    
    Canvas cTitre = new Canvas(200,50);
    GraphicsContext titre = cTitre.getGraphicsContext2D();
    
    titre.fillText("Salut", 10, 10);
    menuGauche.getChildren().add(cTitre);
    menuGauche.setAlignment(Pos.CENTER);
    
    //--------- fin titre ----------
    Button reset = new Button("Reset");
    reset.setOnMouseClicked(e->{
    	listeDePoint.clear();
    	gc.setEffect(null);
    	gc.drawImage(imageReelle, 0, 0);
    });
    menuGauche.getChildren().add(reset);
    
   
   
 // Fin de l'initilisation du menu de gauche
    root.setAlignment(Pos.CENTER);
    root.getChildren().add(menuGauche);
    root.getChildren().add(canvas);
    Scene scene = new Scene(root);
    stage.setTitle("HelloPerspective");
    stage.setScene(scene);
    stage.show();
  }
  
  
 
  

  public static void main(String[] args) {
    Application.launch(args);
  }
}