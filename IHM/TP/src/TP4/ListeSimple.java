package TP4;


import java.io.File;

import javafx.application.Application;
import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;

public class ListeSimple extends Application {
  Label label;
  ListView<String> list1 = new ListView<String>();
  ListView<String> list2 = new ListView<String>();
  String p;

  class MonListChangeListener implements ListChangeListener<String> {
    public void onChanged(javafx.collections.ListChangeListener.Change<? extends String> c){
    	File f = new File("/usr/include/"+c.getList().get(0));
    	list2.getItems().clear();
    	if(f.isDirectory()){
    		
    		System.out.println("42");
    		File path = new File("/usr/include/"+c.getList().get(0));
    	    String[] filelist = path.list();
    	    for (int i = 0; i < filelist.length; i++) {
    	    	p = "/usr/include/"+c.getList().get(0);
   				list2.getItems().add(filelist[i]);
   			}
    	}else{
    		label.setText("Selection de " + c.getList());
    	}label.setText("Selection de "+ c.getList());
      
    }
  }
  class MonListChangeListener2 implements ListChangeListener<String> {
	    public void onChanged(javafx.collections.ListChangeListener.Change<? extends String> c){
	    	label.setText("Selection de " + c.getList());	    	
	    }
  }

  public void start(Stage stage) {
    label = new Label("Aucune selection");

    File path = new File("/usr/include");
    String[] filelist = path.list();
    for (int i = 0; i < filelist.length; i++) {
		list1.getItems().add(filelist[i]);
	}
    list1.setCellFactory(new Callback<ListView<String>,
            ListCell<String>>() {
                @Override
                public ListCell<String> call(ListView<String> list) {
                    return new MonRenduDeCellule();
                }
            }
    );
    list2.setCellFactory(new Callback<ListView<String>,
            ListCell<String>>() {
                @Override
                public ListCell<String> call(ListView<String> list) {
                    return new MonRenduDeCellule2();
                }
            }
    );
   /* list.getItems().addAll("Paris", "Berlin", "Londres", "Rome", "Lisbonne", "Madrid",
                           "New York", "Tokyo", "Pekin");*/
    list1.getSelectionModel().getSelectedItems().addListener(new MonListChangeListener());
    list2.getSelectionModel().getSelectedItems().addListener(new MonListChangeListener2());
    list2.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    
    HBox root = new HBox();
    root.setAlignment(Pos.CENTER_LEFT);
    root.setSpacing(10.0);
    root.setPadding(new Insets(3, 3, 3, 3));
    root.getChildren().addAll(list1,list2,label);


    Scene scene = new Scene(root, 800, 500);
    stage.setTitle("Explorateur");
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    Application.launch(args);
  }
  
  
  
  class MonRenduDeCellule extends ListCell<String> {
	    @Override
	    public void updateItem(String item, boolean empty) {
	      super.updateItem(item, empty);
	      Canvas c = new Canvas(200, 40);
	     
	      GraphicsContext gc = c.getGraphicsContext2D();
	      File f = new File("/usr/include/"+item);
	      Image image;
	      if(f.isDirectory()){
	    	 image = new Image("https://www.iut-info.univ-lille1.fr/~casiez/M2105/TP/TP4EvenementsListView/folder.png");
	      }else{
	    	 image = new Image("https://www.iut-info.univ-lille1.fr/~casiez/M2105/TP/TP4EvenementsListView/file.png");
	      }
	      gc.drawImage(image, 1, 1);
	      gc.fillText(item, 40, 23);
	      setGraphic(c);
	    }
	}
  class MonRenduDeCellule2 extends ListCell<String> {
	    @Override
	    public void updateItem(String item, boolean empty) {
	      super.updateItem(item, empty);
	      Canvas c = new Canvas(200, 40);
	     
	      GraphicsContext gc = c.getGraphicsContext2D();
	      File f = new File(p+"/"+item);
	      Image image;
	      if(f.isDirectory()){
	    	 image = new Image("https://www.iut-info.univ-lille1.fr/~casiez/M2105/TP/TP4EvenementsListView/folder.png");
	      }else{
	    	 image = new Image("https://www.iut-info.univ-lille1.fr/~casiez/M2105/TP/TP4EvenementsListView/file.png");
	      }
	      gc.drawImage(image, 1, 1);
	      gc.fillText(item, 40, 23);
	      setGraphic(c);
	    }
	}
}