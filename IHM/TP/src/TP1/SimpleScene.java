package TP1;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
public class SimpleScene extends Application {

  public void start(Stage stage) {
	
   /* VBox root = new VBox();
    VBox root2 = new VBox();
    Label msg = new Label("Hello JavaFX");
    root.getChildren().add(msg);
    root2.getChildren().add(msg);
   
    Stage s2 = new Stage();
    Scene scene2 = new Scene(root,200,300);
    s2.setScene(scene2);
    s2.setTitle("lel");
   
    Scene scene = new Scene(root2, 300, 50);
    stage.setScene(scene);
    stage.setTitle("Hello JavaFX Application with a Scene");
    
    //stage.setFullScreen(true);
    stage.setAlwaysOnTop(true);
    stage.setResizable(false);
    s2.initModality(Modality.WINDOW_MODAL);
    s2.initOwner(stage);
    stage.setX(300.0);
    stage.setY(500.0);
    
    
    stage.show();
    s2.show();*/
	   /* FlowPane flow = new FlowPane();
	     flow.getChildren().add(new Button("Bouton 1"));
	     flow.getChildren().add(new Button("Bouton 2"));
	     flow.getChildren().add(new Button("Bouton 3"));
	     flow.getChildren().add(new Button("Bouton 4"));
	     flow.getChildren().add(new Button("Bouton 5"));
	     flow.getChildren().add(new Button("Bouton 6"));
	     flow.getChildren().add(new Button("Bouton 7"));
	     flow.getChildren().add(new Button("Bouton 8"));
	     flow.getChildren().add(new Button("Bouton 9"));
	     flow.getChildren().add(new Button("Bouton 10"));

	     Stage s = new Stage();
	     Scene scene = new Scene(flow);
	     s.setScene(scene);
	     s.show();*/
	  BorderPane b = new BorderPane();

	  Button bo = new Button("Ouest");
	  bo.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE );
	  b.setLeft(bo);
	  
	  bo = new Button("Est");
	  bo.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE );
	  b.setRight(bo);
	  
	  bo = new Button("Nord");
	  bo.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE );
	  b.setTop(bo);
	  
	  bo = new Button("Sud");
	  bo.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE );
	  b.setBottom(bo);
	  
	  bo = new Button("Centre");
	  bo.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE );
	  b.setCenter(bo);
	  Stage s = new Stage();
	  Scene scene = new Scene(b);
	  s.setScene(scene);
	  s.show();
	  
	  
	  
	     
  }

  public static void main(String[] args) {
    Application.launch(args);
  }
}