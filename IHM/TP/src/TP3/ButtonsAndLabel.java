package TP3;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ButtonsAndLabel extends Application {
	
	int cpt;
	int x;

        public void start(Stage stage) {
                Label label = new Label(cpt+"");
              
                label.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
                label.setStyle("-fx-background-color: lightblue;"
                            + " -fx-alignment: center;"
                            + " -fx-font: 30px Verdana;");
                Button bmoins = new Button("  -  ");
                Button bplus = new Button("  +  ");

                label.setOnMousePressed(e-> {
                	this.x = (int) e.getX();

                	
                	if(e.isPrimaryButtonDown()){
                		System.out.println("if");
                  		cpt = Integer.parseInt(label.getText()) + 1;
                		label.setText(cpt+"");
                    }
                	else{
                		cpt = Integer.parseInt(label.getText()) - 1;
                		label.setText(cpt+"");
                	}
                });
                
                label.setOnScroll(e->{
                	cpt = (int) (Integer.parseInt(label.getText()) + (e.getDeltaY())/40);
            		label.setText(cpt+"");
                });
                

               label.setOnMouseDragged(e->{
                	
                	if(e.getX()>this.x){
                		cpt = Integer.parseInt(label.getText()) + 1;
                	}else if(e.getX()<this.x){
                		cpt = Integer.parseInt(label.getText()) - 1;
					}
                	label.setText(cpt+"");
                });
                
                bmoins.addEventHandler(ActionEvent.ACTION, e->{
                	cpt = Integer.parseInt(label.getText()) - 1;
                	label.setText(cpt+"");
                });
                
                bplus.setOnAction(e -> {
                	cpt = Integer.parseInt(label.getText()) + 1;
                	label.setText(cpt+"");
                });
                
                VBox vbox = new VBox(3);
                vbox.setPadding(new Insets(3, 3, 3, 3));
                vbox.setAlignment(Pos.CENTER);
                HBox hbox = new HBox(3);
                hbox.getChildren().addAll(bmoins, bplus);
                vbox.getChildren().addAll(label, hbox);

                Scene scene = new Scene(vbox);
                stage.setTitle("Counter");
                stage.setScene(scene);
                stage.show();
        }

        public static void main(String[] args) {
                Application.launch(args);
        }
        
        //---------------------------------
        class myClickHander implements EventHandler<MouseEvent> {
        	
        	Label l;
        	
        	myClickHander(Label l){
        		this.l = l;
        	}
    		
        	public void handle(MouseEvent event) {
        		System.out.println("Class");
    			if(event.isPrimaryButtonDown()){
    				System.out.println("if");
    				int cpt = Integer.parseInt(l.getText()) + 1;
    				l.setText(cpt+"");
    				
    			}if(event.isSecondaryButtonDown()){
    				int cpt = Integer.parseInt(l.getText()) - 1;
    				l.setText(cpt+"");
    				
    			}
    		}

    	}
        //--------
	
}