package TP6;

import javafx.event.ActionEvent;
import javafx.scene.control.Label;

public class MonController {
        
        Label monLabel;

        public void initialize() {
                System.out.println("Initialisation...");
        }

        public void pressedButtonPlus(ActionEvent event) {
                int newValue = Integer.parseInt(monLabel.getText()) + 1;
                monLabel.setText("" + newValue);
        }
        public void pressedButtonMoin(ActionEvent event) {
            int newValue = Integer.parseInt(monLabel.getText()) - 1;
            monLabel.setText("" + newValue);
    }
}