package TP2;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class HelloPaint extends Application {

	GraphicsContext gc;
	int scale = 20;
	ArrayList<Rectangle> l = new ArrayList<Rectangle>();

	class myClickHander implements EventHandler<MouseEvent> {
		public void handle(MouseEvent event) {
			if (event.isShiftDown()) {
				for (int i = 0; i < l.size(); i++) {
						

					System.out.println("shift");
					Point2D p = new Point2D(event.getX(), event.getY());
					if (l.get(i).contains(p)){
						System.out.println("if");
						l.remove(i);

					}
				}
			}else {
				System.out.println("ajout");
				l.add(new Rectangle(event.getX()-10, event.getY()-10, 20, 20));
			}
			repaint();
		}

	}

	class myDragHander implements EventHandler<MouseEvent> {
		public void handle(MouseEvent event) {
			for (int i = 0; i < l.size(); i++) {
				if (l.get(i).contains(event.getX(), event.getY())) {
					System.out.println("drag");
				//	l.remove(i);
				//	l.add(new Rectangle(event.getX() - 10, event.getY() - 10, 20, 20));
					l.get(i).setX(event.getX() - scale/2);
					l.get(i).setY(event.getY() - scale/2);
					repaint();
				}

			}

		}
	}

	public void start(Stage stage) {
		VBox root = new VBox();
		Canvas canvas = new Canvas(300, 300);
		gc = canvas.getGraphicsContext2D();
		
		//root.setOnMouseClicked(new myClickHander());
		root.setOnMouseDragged(new myDragHander());

		

		root.getChildren().add(canvas);
		Scene scene = new Scene(root);
		stage.setTitle("Hello Paint");
		stage.setScene(scene);
		stage.show();
		System.out.println("lel");
		l.add(new Rectangle(0,0,scale,scale));
		l.add(new Rectangle(0,70,scale,scale));
		l.add(new Rectangle(70,70,scale,scale));
		l.add(new Rectangle(70,0,scale,scale));
		repaint();
	}
	
	public void repaint(){

		gc.clearRect(0, 0, 300, 300);
		gc.setFill(Color.RED);
		gc.strokeLine(l.get(0).getX()+scale/2, l.get(0).getY()+scale/2, l.get(1).getX()+scale/2, l.get(1).getY()+scale/2);
		gc.strokeLine(l.get(1).getX()+scale/2, l.get(1).getY()+scale/2, l.get(2).getX()+scale/2, l.get(2).getY()+scale/2);
		gc.strokeLine(l.get(2).getX()+scale/2, l.get(2).getY()+scale/2, l.get(3).getX()+scale/2, l.get(3).getY()+scale/2);
		gc.strokeLine(l.get(3).getX()+scale/2, l.get(3).getY()+scale/2, l.get(0).getX()+scale/2, l.get(0).getY()+scale/2);
		gc.setFill(Color.ORANGE);
		for (Rectangle r : l) {
			gc.fillRect(r.getX(), r.getY(), r.getHeight(), r.getWidth());
		}
		gc.setStroke(Color.BLACK);
		for (Rectangle r : l) {
			gc.strokeRect(r.getX(), r.getY(),  r.getHeight(), r.getWidth());
		}

	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}