
--DROP TABLE IF EXISTS jeu CASCADE;
--DROP TABLE IF EXISTS scores CASCADE;

--CREATE TABLE jeu
--       (id int,
--       nom text,
--       constraint pk_jeu primary Key(id));

--CREATE TABLE scores
--      (login text,
--       id int,
--       score int,
--       constraint fk_jeu foreign Key(id)
--       REFERENCES jeu(id));

--INSERT INTO jeu VALUES(42,'Minecraft');
--INSERT INTO jeu VALUES(43,'League of legens');
--INSERT INTO jeu VALUES(44,'Call of duty');

--INSERT INTO scores VALUES('quentind',42,1200);
--INSERT INTO scores VALUES('quentind',43,1300);
--INSERT INTO scores VALUES('quentind',44,1400);
--INSERT INTO scores VALUES('depommit',42,1201);
--INSERT INTO scores VALUES('depommit',43,1301);
--INSERT INTO scores VALUES('depommit',44,1401);

--CREATE VIEW MesScores
--       AS SELECT scores FROM scores
--       WHERE login = USER;

--CREATE VIEW maximum
--       AS SELECT s.login,j.nom,max(score)
--       FROM scores AS s,jeu AS j
--       WHERE s.id = j.id
--       GROUP BY s.login,j.nom;

--INSERT INTO scores VALUES('depommit',44,1402);
--INSERT INTO scores VALUES('depommit',410,6969);
--INSERT INTO scores VALUES('depommit',410,4242);
--INSERT INTO scores VALUES('quentind',410,1402);


CREATE RULE r1 as on insert to messcores do instead
       insert into scores values(USER,new.id,new.score);


