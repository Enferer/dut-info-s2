DELETE FROM aeroport;
DELETE FROM avion;
DELETE FROM ligne;
DELETE FROM pilote;
DELETE FROM produit;
DELETE FROM vol;

DROP TABLE IF EXISTS noms CASCADE;
DROP TABLE IF EXISTS prenoms CASCADE;
DROP TABLE IF EXISTS adresses CASCADE;
DROP TABLE IF EXISTS compagnies CASCADE;
DROP TABLE IF EXISTS modeles CASCADE;
DROP SEQUENCE IF EXISTS numeros CASCADE;
DROP SEQUENCE IF EXISTS numeros2 CASCADE;

CREATE TEMP TABLE noms
       (nom CHAR(20));
CREATE TEMP TABLE prenoms
       (prenom CHAR(20));
CREATE TEMP TABLE adresses
       (adresse CHAR(50));

INSERT INTO noms(nom) VALUES('Haddock');
INSERT INTO noms(nom) VALUES('Tournesol');
INSERT INTO noms(nom) VALUES('Bergamotte');
INSERT INTO noms(nom) VALUES('Lampion');
INSERT INTO prenoms(prenom) VALUES('Tryphon');
INSERT INTO prenoms(prenom) VALUES('Archibald');
INSERT INTO prenoms(prenom) VALUES('Hippolyte');
INSERT INTO prenoms(prenom) VALUES('Seraphin');
INSERT INTO adresses(adresse) VALUES('Moulinsart');
INSERT INTO adresses(adresse) VALUES('Sbrodj');
INSERT INTO adresses(adresse) VALUES('Bruxelles');
INSERT INTO adresses(adresse) VALUES('Klow');


--SELECT *
--FROM noms,prenoms,addresses;

INSERT INTO pilote (nom,prenom,adresse)
       SELECT * FROM noms,prenoms,adresses;

INSERT INTO avion VALUES(105, 'A320', 300 , 'AIR FRANCE');
INSERT INTO avion VALUES(106, 'A380', 320 , 'LUFTHANSA');
INSERT INTO avion VALUES(107, 'B747', 500 , 'AIR FRANCE');
INSERT INTO avion VALUES(108, 'A320', 300 , 'TWA');
INSERT INTO avion VALUES(109, 'B747', 450 , 'PANAM');
INSERT INTO avion VALUES(110, 'A320', 300 , 'IBERNAMIA');

CREATE TABLE compagnies
       (compagnie CHAR(25));
INSERT INTO compagnies (compagnie)
       SELECT DISTINCT compagni FROM avion;

CREATE TABLE modeles
       (type CHAR(5),places INT);

INSERT INTO modeles(type,places)
       SELECT type,MAX(places) FROM avion GROUP BY type;

CREATE SEQUENCE numeros START 130;

INSERT INTO avion(ano,compagni,type,places)
       SELECT nextval('numeros'),compagnies,type,places
       FROM compagnies,modeles;


INSERT INTO aeroport(rcode, libelle, ville)
VALUES ('JFK', 'John Fitzgerald Kennedy', 'New-York') ;
INSERT INTO aeroport(rcode, libelle,ville)
VALUES ('CDG', 'Roissy Charles de Gaulles','Paris') ;
INSERT INTO aeroport(rcode, libelle,ville)
VALUES ('MAD', 'Madrid Barajas','Madrid') ;
INSERT INTO aeroport(rcode, libelle) VALUES ('BRU', 'Bruxelles') ;
INSERT INTO aeroport(rcode, libelle) VALUES ('GVA', 'Geneve') ;
INSERT INTO aeroport(rcode, libelle, ville)
VALUES ('ORY', 'Orly', 'Paris') ;
INSERT INTO aeroport(rcode, libelle) VALUES ('LAI', 'Lannion') ;
INSERT INTO aeroport(rcode, libelle, ville)
VALUES ('LIL', 'Lille-Lesquin', 'Lille') ;

UPDATE aeroport
SET ville = libelle
WHERE ville IS NULL;


CREATE SEQUENCE numeros2 START 1000;

INSERT INTO ligne(lno,depart,arrivee)
SELECT nextval('numeros2'),a1.rcode,a2.rcode

FROM aeroport AS a1 JOIN aeroport AS a2 ON a1.rcode <> a2.rcode;

INSERT INTO vol
       (SELECT ano,pno,lno
       FROM ligne,pilote,avion
       WHERE (depart IN('JFK','MAD','CDG') OR arrivee IN('MAD','JFK','CDG'))
       AND pno % 7 = 0
       AND compagni LIKE 'AIR FRANCE%'
       AND type LIKE 'A%');
