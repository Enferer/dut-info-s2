DROP TABLE IF EXISTS commande;
DROP TABLE IF EXISTS fournisseur;
DROP TABLE IF EXISTS produit;


CREATE TABLE fournisseur
	(fno SERIAL,
	nom CHAR(25),
	prenom CHAR(25),
	adresse CHAR(120),
	tel TEXT,
	CONSTRAINT pk_fournisseur PRIMARY KEY (fno) );

CREATE TABLE produit
	(pno SERIAL,
	libelle CHAR(50),
	couleur CHAR(20),
	poids INTEGER,
	CONSTRAINT pk_produit PRIMARY KEY (pno) );

CREATE TABLE commande
	(fno INTEGER,
	pno INTEGER,
	prix INTEGER,
	qute INTEGER,
	CONSTRAINT fk_produit FOREIGN KEY(pno)
	REFERENCES produit(pno),
	CONSTRAINT fk_fournisseur FOREIGN KEY (fno)
	REFERENCES fournisseur (fno) );

INSERT INTO fournisseur (nom,prenom)
	VALUES ('Dupont','paul') ;
	
INSERT INTO fournisseur (nom,prenom)
	VALUES ('Durant','pierre') ;

INSERT INTO produit (libelle,couleur)
	VALUES ('Chaise','rouge') ;

INSERT INTO produit (libelle,couleur)
	VALUES ('Bureau','gris') ;

INSERT INTO commande VALUES(1,1,50,2);
INSERT INTO commande VALUES(1,3,25,1);
INSERT INTO commande VALUES(2,1,30,10);
INSERT INTO commande VALUES(2,2,150,4);
