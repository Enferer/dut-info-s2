DROP TABLE IF EXISTS avion CASCADE;
DROP TABLE IF EXISTS pilote CASCADE;
DROP TABLE IF EXISTS aeroport CASCADE;
DROP TABLE IF EXISTS ligne CASCADE;
DROP TABLE IF EXISTS vol CASCADE;

CREATE TABLE avion
       (ano INTEGER,
       type CHAR(4),
       places INTEGER CHECK(places BETWEEN 100 AND 500),
       compagni TEXT,
       CONSTRAINT pk_avion PRIMARY KEY (ano) );
CREATE TABLE pilote
       (pno SERIAL,
       nom char(10) not null,
       prenom char(25),
       adresse char(200),
       CONSTRAINT pk_pilote primary key ( pno ) );


CREATE TABLE aeroport
       (rcode CHAR(3),
       libelle TEXT,
       ville TEXT,
       adresseA TEXT,
       CONSTRAINT pk_aeroport PRIMARY KEY (rcode) );

CREATE TABLE ligne
       (lno INTEGER,
       depart CHAR(3),
       arrivee CHAR(3),
       CONSTRAINT pk_ligne PRIMARY KEY(lno),
       CONSTRAINT fk_aeroport1 foreign KEY(depart)
       		  references aeroport(rcode)
		  on update cascade on delete set null,

       CONSTRAINT fk_aeroport2 foreign KEY(arrivee)
		  references aeroport(rcode)
		  on update cascade on delete set null);

CREATE TABLE vol
       (ano INTEGER,
       pno INTEGER,
       lno INTEGER,
       hdep TIMESTAMP,
       harr TIMESTAMP,
       CONSTRAINT pk_vol primary key(ano,pno,lno),
       CONSTRAINT pk_avion foreign key(ano)
       		  references avion (ano)
		  on update cascade on delete cascade,
       CONSTRAINT pk_pilote foreign key ( pno )
       		  references pilote ( pno )
		  on update cascade on delete cascade,
       CONSTRAINT pk_ligne foreign key ( lno )
       		  references ligne ( lno )
		  on update cascade on delete cascade);


INSERT INTO avion VAlues(100, 'B747',350,'AIR FRANCE');
INSERT INTO avion VAlues(101, 'B747',600,'TWA');		 
INSERT INTO avion (ano,type,places) VAlues(102, 'A320',200);		  
INSERT INTO avion (ano,type,compagnie) VAlues(103, 'B747','TWA');
INSERT INTO avion (compagnie,places,type,ano) VAlues('AIR FRANCE',500,'B747',104);
INSERT INTO avion (places,type) VAlues(350,'B747');
INSERT INTO aeroport VALUES('CDG','Roissy-Charles de Gaulle','paris','roissy');
INSERT INTO aeroport(rcode, libelle,ville) VALUES ( 'CDG','Roissy-Charles de Gaulle','Paris');
INSERT INTO aeroport(rcode , ville , libelle) VALUES ( 'MAD','Madrid Barajas','madrid');
INSERT INTO aeroport(rcode , ville ) VALUES ( 'SDN','sydney');
INSERT INTO pilote(nom,prenom,adressep) VALUES('Szut','Piotr','Estonie');
INSERT INTO pilote VALUES('Lindbergh','Charles','USA');
INSERT INTO pilote(nom,prenom) VALUES('Abagnale','Frank');
INSERT INTO pilote(nom,prenom) VALUES('Abagnale','Frank')
