DROP TABLE IF EXISTS acteur CASCADE;
DROP TABLE IF EXISTS spectacle CASCADE;
DROP TABLE IF EXISTS jouer CASCADE;
DROP TABLE IF EXISTS representation CASCADE;

CREATE TABLE acteur 
	(nom_acteur TEXT,
		CONSTRAINT pk_acteur PRIMARY KEY(nom_acteur));

CREATE TABLE spectacle
	(num_spec INTEGER,
	titre_spec TEXT,
	salle TEXT,
	monteur TEXT,
	CONSTRAINT pk_spectacle PRIMARY KEY(num_spec));

CREATE TABLE jouer
	(nom_acteur TEXT,
	num_spec INTEGER,
	CONSTRAINT pk_jouer PRIMARY KEY(nom_acteur,num_spec),
	CONSTRAINT fk_nom_acteur FOREIGN KEY(nom_acteur)
		REFERENCES acteur(nom_acteur),
	CONSTRAINT fk_num_spec FOREIGN KEY(num_spec)
		REFERENCES spectacle(num_spec));

CREATE TABLE representation
	(date TEXT,
	heure TEXT,
	num_spec INTEGER,
	tarif INTEGER,
	CONSTRAINT pk_representation PRIMARY KEY(date,heure,num_spec),
	CONSTRAINT fk_num_spec FOREIGN KEY(num_spec)
		REFERENCES spectacle(num_spec));

INSERT INTO acteur VALUES('spankbang');
INSERT INTO acteur VALUES('Noiret');
INSERT INTO acteur VALUES('Auteuil');
INSERT INTO acteur VALUES('Rochefort');
INSERT INTO acteur VALUES('Delon');
INSERT INTO acteur VALUES('Belmondo');
INSERT INTO acteur VALUES('Galabru');
INSERT INTO acteur VALUES('Blanc');
INSERT INTO acteur VALUES('Lefebvre');

INSERT INTO spectacle VALUES(10,'Pinocchio','Chopin','Durand');
INSERT INTO spectacle VALUES(20,'Bambi','Debussy','Dupont');
INSERT INTO spectacle VALUES(30,'Le roi lion','Chopin','Lefebvre');
INSERT INTO spectacle VALUES(40,'Peter Pan','Debussy','Dupont');
INSERT INTO spectacle VALUES(50,'Robin des bois','Claudel','Durand');
INSERT INTO spectacle VALUES(60,'Pocahontas','Chopin','Lefebvre');

INSERT INTO jouer VALUES('Depardieu',10);
INSERT INTO jouer VALUES('Noiret',20);
INSERT INTO jouer VALUES('Auteuil',10);
INSERT INTO jouer VALUES('Rochefort',40);
INSERT INTO jouer VALUES('Delon',50);
INSERT INTO jouer VALUES('Belmondo',40);
INSERT INTO jouer VALUES('Noiret',10);


INSERT INTO representation VALUES('2014-10-12','16:00',10,20);
INSERT INTO representation VALUES('2014-10-12','20:00',60,30);
INSERT INTO representation VALUES('2014-10-12','16:00',20,15);
INSERT INTO representation VALUES('2014-10-12','20:00',10,20);
INSERT INTO representation VALUES('2014-10-12','16:00',50,35);
INSERT INTO representation VALUES('2014-10-12','20:00',40,35);
INSERT INTO representation VALUES('2014-10-13','20:00',60,20);





