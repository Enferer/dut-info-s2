package entrepotSim;

public class LiquideNonDangeureux extends Liquide{

	public LiquideNonDangeureux(String nom) {
		super(nom);

	}


	public boolean estConditionne() {
		return false;
	}


	public boolean estLiquide() {
		return true;
	}


	public boolean estDangeureux() {
		return false;
	}

}
