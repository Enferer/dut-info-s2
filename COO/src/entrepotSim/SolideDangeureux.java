package entrepotSim;

public class SolideDangeureux extends Solide {
	private final boolean corosif,inflamable;

	public SolideDangeureux(String nom2, boolean coro, boolean infla) {
		super(nom2);
		corosif=coro;
		inflamable=infla;
	}
	public boolean estCorosif() {
		return corosif;
	}

	public boolean estInflamable() {
		return inflamable;
	}

	public boolean estConditionne() {
		return true;
	}

	public boolean estLiquide() {
		return false;
	}

	public boolean estDangeureux() {
		return true;
	}

}
