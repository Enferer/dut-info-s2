package entrepotSim;

public abstract class Liquide extends Produit {
	private int volume;
	public Liquide(String nom) {
		super(nom);
	}
	public Liquide(String nom,int vol){
		this(nom);
		volume=vol;
	}
	public int getVolume(){
		return volume;
	}
	public void setVolume(int vol){
		volume=vol;
	}

}
