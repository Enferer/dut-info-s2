package entrepotSim;

public abstract class Produit {
	private String nom;

	public Produit(String nom2) {
		nom=nom2;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public abstract boolean estConditionne();
	public abstract boolean estLiquide();
	public abstract boolean estDangeureux();
	
}
