package entrepotSim;

public class Bidon extends ConteneurALiquide {
	private LiquideNonDangeureux liquideContenu;
	
	Bidon(int volume) {
		super(volume);
	}

	public Liquide getLiquideContenu() {
		return liquideContenu;
	}

	public void setLiquideContenu(LiquideNonDangeureux liquideContenu) {
		this.liquideContenu = liquideContenu;
	}

	
	@Override
	public int peutContenir(Produit p) {
		if(p.estDangeureux()){
			return 0;
		}
		if(!p.estLiquide()){
			return 0;
		}
		if(this.getLiquideContenu()==null){
			return this.getVolumeMax();
		}else{
			if(!this.getLiquideContenu().equals((LiquideNonDangeureux)p)){
				return 0;
			}else{
				return this.getVolumeMax()-this.getVolumeDeLiquideContenu();
			}
		}
	}

	@Override
	public void ajout(Produit p, int quantite) {
		this.setLiquideContenu((LiquideNonDangeureux)p);
		this.setVolumeDeLiquideContenu(quantite+this.getVolumeDeLiquideContenu());
		
	}	
	
	
	
	
}
