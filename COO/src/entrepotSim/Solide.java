package entrepotSim;

public abstract class Solide extends Produit {
	private int masse;
	public Solide(String nom2) {
		super(nom2);
		masse=0;
	}
	public Solide(String nom,int ma){
		this(nom);
		masse=ma;
	}
	public int getMasse() {
		return masse;
	}
	public void setMasse(int masse) {
		this.masse = masse;
	}
	

}
