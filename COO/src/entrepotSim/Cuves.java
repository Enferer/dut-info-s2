package entrepotSim;

public class Cuves extends ConteneurALiquide {
	private Liquide liquide;
	
	Cuves(int volume) {
		super(volume);
	}

	@Override
	public Liquide getLiquideContenu() {
		return liquide;
	}

	public void setLiquide(Liquide liquide) {
		this.liquide = liquide;
	}

	@Override
	public int peutContenir(Produit p) {
		if(!p.estLiquide()){
			return 0;
		}
		if(this.getLiquideContenu()==null){
			return this.getVolumeMax();
		}else{
			if(!this.getLiquideContenu().equals((Liquide)p)){
				return 0;
			}else{
				return this.getVolumeMax()-this.getVolumeDeLiquideContenu();
			}
		}
	}

	@Override
	public void ajout(Produit p, int quantite) {
		this.setLiquide((Liquide)p);
		this.setVolumeDeLiquideContenu(quantite+this.getVolumeDeLiquideContenu());
		
	}
	
}
