package entrepotSim;

public abstract class ConteneurALiquide extends Containers {
	private final int volumeMax;
	private int volumeDeLiquideContenu;
	private Liquide contenu;
	
	
	ConteneurALiquide(int volume){
		this.volumeMax=volume;
		contenu=null;
		volumeDeLiquideContenu=0;
	}
	
	public int getVolumeMax() {
		return volumeMax;
	}
	
	
	public int getVolumeDeLiquideContenu() {
		return volumeDeLiquideContenu;
	}

	public void setVolumeDeLiquideContenu(int volumeDeLiquideContenu) {
		this.volumeDeLiquideContenu = volumeDeLiquideContenu;
	}

	public Liquide getLiquideContenu(){
		return contenu;
	}
	
	
}
