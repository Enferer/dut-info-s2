package entrepotSim;

public class LiquideDangeureux extends Liquide implements Dangeureux {
	private final boolean corosif,inflamable;
	
	public LiquideDangeureux(boolean coro,boolean infla,String nom){
		super(nom);
		corosif=coro;
		inflamable=infla;
	}
	public boolean estCorosif() {
		return corosif;
	}

	public boolean estInflamable() {

		return inflamable;
	}
	@Override
	public boolean estConditionne() {

		return false;
	}
	@Override
	public boolean estLiquide() {
		return true;
	}
	@Override
	public boolean estDangeureux() {
		return true;
	}

}
