package entrepotSim;

public interface Dangeureux {
	public boolean estCorosif();
	public boolean estInflamable();
}
