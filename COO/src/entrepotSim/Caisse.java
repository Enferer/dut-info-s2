package entrepotSim;

import java.util.ArrayList;

public class Caisse  extends Containers{
	private final int masseMax;
	private ArrayList<Solide> contenu;
	private boolean accepteDangeureux;
	
	public Caisse(int masseMx,boolean dangeureuxAutorise){
		accepteDangeureux=dangeureuxAutorise;
		this.masseMax=masseMx;
		contenu=new ArrayList<Solide>();
	}


	public int getMasseMax() {
		return masseMax;
	}




	@Override
	public int peutContenir(Produit p) {
		if(p.estLiquide()){
			return 0;
		}else{
			if(accepteDangeureux && p.estDangeureux()){
				if(contientDejaUnDangeureuxInflamable()){
					return 0;
				}
			}
			return masseRestante();
			
		}
	}


	@Override
	public void ajout(Produit p, int quantite) {
		for(int i=0;i<quantite;i++){
			contenu.add((Solide)p);
		}
		
	}
	public boolean contientDejaUnDangeureuxInflamable(){
		for(Produit p:contenu){
			if(p.estDangeureux()){
				SolideDangeureux sop=(SolideDangeureux)p;
				if(sop.estInflamable()){
				return true;
				}
			}
		}
		return false;
	}
	
	public int masseRestante(){
		int masseActu=0;
		for(Solide s:contenu){
			masseActu+=s.getMasse();
		}
		return masseActu;
	}
}
