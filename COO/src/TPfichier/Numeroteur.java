package TPfichier;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

public class Numeroteur {

	private static int no = 0;
	
	public static void main(String[] args) {
		int cpt=1;
		String ligne;
		FileReader flux;
		BufferedReader fichier;
		

		try{
			//PrintWriter text = new PrintWriter(args[0].split(".")[0]+"."+no+".txt");
			PrintWriter text = new PrintWriter("test.txt");
			flux=new FileReader(args[0]); 
			fichier=new BufferedReader(flux);
			while ((ligne=fichier.readLine())!=null){
				text.write(cpt++ + "\t"+ligne+"\n");
				//text.print();
				System.out.println(cpt + "\t"+ligne);
			}
			text.flush();
			flux.close();
			fichier.close(); 
		}
		catch (Exception e){
			System.err.println("Erreur ==> "+e.toString());
		}
	}
}