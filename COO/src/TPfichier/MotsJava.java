package TPfichier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

public class MotsJava {

	
	public static void main(String[] args) {
		String ligne;
		FileReader fileReader;
		BufferedReader bufferedReader;

		try{
			PrintWriter printWriter = new PrintWriter("dico.txt");
			fileReader=new FileReader(args[0]); 
			bufferedReader=new BufferedReader(fileReader);
			ligne=bufferedReader.readLine();
			while (ligne!=null){
				String[]tab = ligne.split(" ");
				printWriter.write(tab[0]+"\n");
				ligne=bufferedReader.readLine();
			}
			printWriter.flush();
			printWriter.close();
			fileReader.close();
			bufferedReader.close(); 
		}
		catch (Exception e){
			System.err.println("Erreur ==> "+e.toString());
		}
	}
	
}
