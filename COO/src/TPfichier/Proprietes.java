package TPfichier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

public class Proprietes {

	public static void main(String[] args) {
		String ligne;
		FileReader flux;
		BufferedReader fichier;

		try{
		
			flux=new FileReader(args[0]); 
			fichier=new BufferedReader(flux);
			while ((ligne=fichier.readLine())!=null){
				if(!(ligne.charAt(0) == '#')){
					System.out.println(ligne);
				}
			}
			flux.close();
			fichier.close(); 
		}
		catch (Exception e){
			System.err.println("Erreur ==> "+e.toString());
		}
	}
	
	
}
