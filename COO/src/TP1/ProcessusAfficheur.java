package TP1;

public class ProcessusAfficheur implements Processus,Prioritaire{

	int priorite = 3;
	String message;
	int nbrExec = 1;
	
	public ProcessusAfficheur(String message) {
		super();
		this.message = message;
	}

	public int getPriorite() {
		// TODO Auto-generated method stub
		return priorite;
	}

	@Override
	public void execute() {
		System.out.println(message);
		nbrExec--;
		
		
	}

	@Override
	public boolean estFini() {
		return nbrExec == 0;
	}

}
