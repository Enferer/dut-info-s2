package TP1;

import java.util.ArrayList;

public class OrdoFile implements Ordonnanceur{

	
	ArrayList<Prioritaire> l = new ArrayList<Prioritaire>();
	
	public void ajouteObjet(Prioritaire p) {
		l.add(p);
	}

	public Prioritaire plusPrioritaire() {
		for (Prioritaire p : l) {
			return p;
		}
		return null;
	}

	public boolean estVide() {
		return l.size() <= 0;
	}

}
