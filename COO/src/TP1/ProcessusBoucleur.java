package TP1;

public class ProcessusBoucleur implements Processus,Prioritaire{

	int priorite = 2;
	String message;
	int n;
	int nbrExec = 3;
	
	
	
	public ProcessusBoucleur(String message, int n) {
		super();
		this.message = message;
		this.n = n;
	}

	@Override
	public int getPriorite() {
		return priorite;
	}

	@Override
	public void execute() {
		for (int i = 0; i < this.n; i++) {
			System.out.println(message);
		}
		
	}

	@Override
	public boolean estFini() {
		return nbrExec == 0;
	}

}
