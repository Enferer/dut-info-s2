package TP1;

public class OrdoAvecPriorite {

	private OrdoFile[] tab;
	private int nbrMax;
	
	public OrdoAvecPriorite(int nbrMax){
		this.nbrMax = nbrMax;
		tab = new OrdoFile[nbrMax];
		for (int cpt = 0 ; cpt < tab.length ; cpt++) {
			tab[cpt] = new OrdoFile();
		}
	}
	
	public boolean ajouteObjet(Prioritaire p){	
		if(p.getPriorite() >= 0 && p.getPriorite() < nbrMax){
			tab[p.getPriorite()].ajouteObjet(p);
			return true;
		}
		return false;
		
	}
	
	public Prioritaire plusPrioritaire(){
		
		for (int cpt = 0 ; cpt < tab.length ; cpt++) {
				return tab[cpt].plusPrioritaire();
		}
		return null;
	}
}