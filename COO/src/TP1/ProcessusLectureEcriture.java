package TP1;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ProcessusLectureEcriture implements Processus,Prioritaire{

	
	int priorite = 1;
	String message;
	
	int nbrExec = 2;

	@Override
	public int getPriorite() {
		return priorite;
	}

	@Override
	public void execute() {
		if(nbrExec == 2 ){
			JFrame frame = new JFrame();
			message = JOptionPane.showInputDialog(frame,"rentrez un message");
			nbrExec--;
		}else{
			System.out.println(message);
			nbrExec--;
		}
	}

	@Override
	public boolean estFini() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
}
