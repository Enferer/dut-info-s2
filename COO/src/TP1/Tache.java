package TP1;

public class Tache implements Prioritaire{

	
	private String tache = "";
	private int priorite;
	


	public Tache(String tache, int priorite) {
		super();
		this.tache = tache;
		this.priorite = priorite;
	}

	public int getPriorite() {
		return priorite;
	}
	public String getTache(){
		return tache;
	}
	
	public void setTache(String tache) {
		this.tache = tache;
	}

	public void setPriorite(int priorite) {
		this.priorite = priorite;
	}	

	public String toString() {
		return "Tache [tache=" + tache + ", priorite=" + priorite + "]";
	}

	public static void main(String[] args) {
		/*OrdoFile o = new OrdoFile();
		o.ajouteObjet(new Tache("un",0));
		o.ajouteObjet(new Tache("deux",0));
		o.ajouteObjet(new Tache("trois",0));
		o.ajouteObjet(new Tache("quatre",0));
		System.out.println(o.plusPrioritaire());*/
		
		/*OrdoAvecPriorite o = new OrdoAvecPriorite(2);
		o.ajouteObjet(new Tache("un",0));		
		o.ajouteObjet(new Tache("deux",1));
		o.ajouteObjet(new Tache("trois",2));
		o.ajouteObjet(new Tache("quatre",3));
		System.out.println(o.plusPrioritaire());*/
		
		ProcessusAfficheur p1 = new ProcessusAfficheur("team 42 c'est pas si mal");
		ProcessusBoucleur p2 = new ProcessusBoucleur("salut",2);
		ProcessusLectureEcriture p3 = new ProcessusLectureEcriture();
		
		OrdoAvecPriorite o = new OrdoAvecPriorite(5);
		o.ajouteObjet(p1);
		o.ajouteObjet(p2);
		o.ajouteObjet(p3);

		Processus p = (Processus) o.plusPrioritaire();
		p.execute();
	}
}

