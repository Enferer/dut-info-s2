package PacMan;


import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

/**
 * Classe illustarnt la création d'une fenêtre contenant un rectangle et une
 * oval
 * 
 * @author M2014
 *
 */
public class Test extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur de la fenêtre
	 * 
	 * @param titre
	 *            titre de la fenêtre
	 * @param x
	 *            abscisse coin supérieur gauche de la fenêtre
	 * @param y
	 *            ordonnée coin supérieur gauche de la fenêtre
	 * @param largeur
	 *            largeur de la fenêtre
	 * @param hauteur
	 *            hauteur de la fenêtre
	 */
	public Test(String titre, int x, int y, int largeur, int hauteur) {
		super(titre);
		this.setLocation(x, y);
		this.setSize(largeur, hauteur);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // fermeture
																// automatique
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.Window#paint(java.awt.Graphics)
	 */
	public void paint(Graphics g) {
		g.setColor(Color.GREEN); // fixe la couleur courante
		g.fillRect(50, 60, 90, 55); // dessine un rectangle plein

		g.setColor(Color.BLACK); // modifie la couleur courante
		g.drawOval(55, 70, 80, 35); // dessine un oval en mode filaire
	}

	/**
	 * Crée une instance de la fenêtre
	 * 
	 * @param args inutilisé
	 */
	public static void main(String[] args) {
		new Test("Oh!", 300, 160, 200, 150);
	}
}