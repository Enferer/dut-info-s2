package PacMan;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;

public class Feu extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int cpt ; 	
	//private Color couleur1=Color.RED, couleur2=Color.GREEN; 
	
	/**
	 * Constructeur de la fenÃªtre
	 * @param titre	 titre de la fenÃªtre
	 * @param x abscisse coin supÃ©rieur gauche de la fenÃªtre
	 * @param y ordonnÃ©e coin supÃ©rieur gauche de la fenÃªtre
	 * @param largeur largeur de la fenÃªtre
	 * @param hauteur hauteur de la fenÃªtre
	 */
	public Feu(String titre,int x,int y, int largeur,int hauteur) {		
		super(titre);
		this.setSize( largeur, hauteur );
		this.setVisible( true );
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			
		this.setLocation(x,y);
	}
					
	/**
	 * 
	 */
	public void animer (){	
		while (true){
			this.repaint();			
		}
	}
		
	/* (non-Javadoc)
	 * @see java.awt.Window#paint(java.awt.Graphics)
	 */
	public void paint (Graphics g){
		if(cpt%3 == 2){
			g.setColor( Color.red);
			g.fillArc(70, 60, 50, 50,0, 360);
			g.setColor( Color.black);
			g.fillArc(70, 125, 50, 50, 0, 360);
			g.setColor( Color.black);
			g.fillArc(70, 190, 50, 50,0, 360);
			try { Thread.sleep(2000l); } 
			catch (InterruptedException e) { e.printStackTrace(); }
			this.cpt++;
		}
		if(cpt%3 == 1){
			g.setColor( Color.black);
			g.fillArc(70, 60, 50, 50,0, 360);
			g.setColor( Color.orange);
			g.fillArc(70, 125, 50, 50, 0, 360);
			g.setColor( Color.black);
			g.fillArc(70, 190, 50, 50,0, 360);
			try { Thread.sleep(500l); } 
			catch (InterruptedException e) { e.printStackTrace(); }
			this.cpt++;
		}
		if(cpt%3 == 0){
			g.setColor( Color.black);
			g.fillArc(70, 60, 50, 50,0, 360);
			g.setColor( Color.black);
			g.fillArc(70, 125, 50, 50, 0, 360);
			g.setColor( Color.green);
			g.fillArc(70, 190, 50, 50,0, 360);
			try { Thread.sleep(2000l); } 
			catch (InterruptedException e) { e.printStackTrace(); }
			this.cpt++;
		}
	}
		
		
	
	
	/**
	 * CrÃ©e une instance de la fenÃªtre
	 * @param args inutilisÃ©
	 */
	public static void main(String[] args) {		
		 new Feu("Feu", 150, 150,200,350).animer();
	}	
}
