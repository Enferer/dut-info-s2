package PacMan;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JFrame;

public class PacMan extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int cpt ;
	private int taille;
	private int orientation;
	private int x = 30;
	private int y = 30;
	private int vitesse;
	

	public PacMan(String titre,int x,int y, int largeur,int hauteur, int vitesse) {		
		super(titre);
		this.vitesse = vitesse;
		this.taille = largeur;
		this.setSize( taille, taille );
		this.setVisible( true );
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			
		this.setLocation(x,y);
	}
					
	/**
	 * 
	 */

		
	public void paint (Graphics g){
		g.setColor( Color.black);
		g.fillRect(0, 0, taille, taille);
		orientation = 0;
		Random r = new Random();
		int xPoint = r.nextInt(taille-60)+30;
		int yPoint = r.nextInt(taille-60)+30;
		while(true){
			
			if( this.x >= xPoint-10 && this.x <= xPoint+10 && this.y >= yPoint-10 && this.y <= yPoint+10){
				g.setColor( Color.black);
				g.fillRect(0, 0, taille, taille);
				xPoint = r.nextInt(taille-60)+30;
				yPoint = r.nextInt(taille-60)+30;
				g.setColor( Color.white);
				g.fillArc(xPoint, yPoint, 25, 25, 0, 360);
			}
			if(x+60 >= taille &&  y+60 > 0){
				orientation = 1;
			}
			if(x+60 >= 500 && y+60 >= taille){
				orientation = 2;
			}
			if(x-10 <= 0){
				orientation = 3;
			}
			if(x-10 <= 0 && y-30 <= 0){
				orientation = 0;
			}
			g.setColor( Color.black);
			g.fillRect(0, 0, taille, taille);
			g.setColor(Color.white);
			g.fillArc(xPoint, yPoint, 25, 25, 0, 360);
			if(orientation == 0){
				x = x+10;
				g.setColor( Color.black);
				g.fillArc(x, y, 50, 50,0, 360);
				g.setColor( Color.yellow);
				g.fillArc(x, y, 50, 50, 35,300 );
				try { Thread.sleep(this.vitesse); } 
				catch (InterruptedException e) { e.printStackTrace(); }
				g.setColor( Color.yellow);
				g.fillArc(x, y, 50, 50, 0,360 );
				try { Thread.sleep(this.vitesse); } 
				catch (InterruptedException e) { e.printStackTrace(); }
				cpt++;
			}
			if(orientation == 1){
				y = y+ 10;
				g.setColor( Color.black);
				g.fillArc(x, y, 50, 50,0, 360);
				g.setColor( Color.yellow);
				g.fillArc(x, y, 50, 50, 300,300 );
				try { Thread.sleep(this.vitesse); } 
				catch (InterruptedException e) { e.printStackTrace(); }
				g.setColor( Color.yellow);
				g.fillArc(x, y, 50, 50, 0,360 );
				try { Thread.sleep(this.vitesse); } 
				catch (InterruptedException e) { e.printStackTrace(); }
				cpt++;
			}
			if(orientation == 2){
				x = x - 10; 
				g.setColor( Color.black);
				g.fillArc(x, y, 50, 50,0, 360);
				g.setColor( Color.yellow);
				g.fillArc(x, y, 50, 50, 205,300 );
				try { Thread.sleep(this.vitesse); } 
				catch (InterruptedException e) { e.printStackTrace(); }
				g.setColor( Color.YELLOW);
				g.fillArc(x, y, 50, 50, 0,360 );
				try { Thread.sleep(this.vitesse); } 
				catch (InterruptedException e) { e.printStackTrace(); }
				cpt++;
			}
			if(orientation == 3){
				y = y - 10;
				g.setColor( Color.black);
				g.fillArc(x, y, 50, 50,0, 360);
				g.setColor( Color.yellow);
				g.fillArc(x, y, 50, 50, 110,300 );
				try { Thread.sleep(this.vitesse); } 
				catch (InterruptedException e) { e.printStackTrace(); }
				g.setColor( Color.YELLOW);
				g.fillArc(x, y, 50, 50, 0,360 );
				try { Thread.sleep(this.vitesse); } 
				catch (InterruptedException e) { e.printStackTrace(); }
				cpt++;
			}
			
		}

	}
		
		
	public static void main(String[] args) {
		 new PacMan("PacMan", 150, 150,500,500,50);
	}	
}
