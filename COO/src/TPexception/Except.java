package TPexception;
class Controle {
	
	void verif_1(String s) { // capture locale
		try { 
			if (Integer.parseInt(s)%2==0) { 
				throw new Exception("Bing! ");
			} 
		}catch(Exception e) { 
			System.out.print("Bang! "); 
		}finally{ 
			System.out.print("Bung! ");
	
		}
	
	}
	
	void verif_2(String s) throws Exception{ // propagation
		if (Integer.parseInt(s)%2==0) { 
			throw new Exception("Bong!"); 
		}
	}
}

public class Except {
	public static void main(String args[]) {		
		try {
			new Controle().verif_1(args[0]);
			System.out.println();
			new Controle().verif_2(args[1]);
		}
		catch(Exception e) { System.out.print("Beng ! "+e); }
	}
}

