package TPexception;
import java.util.Random;

public class Division {
	
	public static void main(String[] args){
		Random r=new Random();
		int division, numerateur=r.nextInt(10),denominateur=r.nextInt(10);
		
		try { 
			System.out.print("Division : "+numerateur+"/"+denominateur+"=");
			division= numerateur/denominateur; 
			}
		catch(Exception e) { System.out.println("Erreur"); }
		finally { division=-1; }
		System.out.println(division);
	}
}