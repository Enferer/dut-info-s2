package TPexception;
import java.io.FileInputStream;
import java.io.IOException;

public class Exemple {
	public static void main(String[] args) {

		String nom = args[0];
		String nombre = args[1];
		FileInputStream fichier;

		try {
			System.out.println("#1");
			Integer n = new Integer(nombre);
			System.out.println("#2");
			fichier = new FileInputStream(nom);			
			System.out.println("#3");
		}
		catch (IOException e) {
			System.out.println("Impossible d'ouvrir le fichier : "+nom);
		}
		catch (NumberFormatException e) {
			System.out.println(nombre+" n'est pas un nombre entier");
		}
		finally {
			System.out.println("#4");
		}
	}
}