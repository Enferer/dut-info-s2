package TPexception;

import java.io.FileInputStream;
import java.io.IOException;

public class Exceptions {
	public static void main(String args[]) {
		int x = 1;
		int y = 1;
		int z[] = new int[3];
		try {
			// Runtime Exceptions
			System.out.println(" x/y=" + (x / y));
			System.out.println(" z[x]=" + z[x]);
			// Checked Exception
			FileInputStream f = new FileInputStream("fichier.txt");
			// Error Exception
			int M = 100000;
			Double[] d = new Double[M * M];
		} 
		catch (IOException e) {
			System.out.println("Erreur E/S");
		} 
		catch (ArithmeticException e) {
			System.out.println("Erreur ?/0");
		} 
		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Erreur z[-?]");
		} 
		catch (OutOfMemoryError e) {
			System.out.println("Erreur ");
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("ca continue…");
	}
}