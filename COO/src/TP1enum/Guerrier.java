package TP1enum;

public class Guerrier extends Personnage implements Combattant{

	Arme a;
	
	public Guerrier(){
		super();
	}
	public Guerrier(String nom){
		super(nom);
	}
	public Guerrier(String nom,Arme a){
		super(nom);
		this.a = a;
	}
	public void attaque(Combattant c ){
		super.attaque((Vivant) c );
	}

	public void subitAttaque(Combattant c) {
		super.subitAttaque((Vivant) c);
		
	}
	public Arme getArme() {
		return a;
		
	}
	public String toString() {
		return "Guerrier [a=" + a + ", toString()=" + super.toString() + "]";
	}



	
}
