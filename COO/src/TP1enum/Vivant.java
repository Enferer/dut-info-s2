package TP1enum;

import java.util.Random;

public interface Vivant {

	public final static Random r = new Random();
	
	void attaque(Vivant v);
	void subitAttaque(Vivant v);
	void perte(int i);
	int getForce();
	boolean estVivant();
	
}
