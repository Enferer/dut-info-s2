package TP1enum;

public class Elfe extends Personnage implements Magicien,Combattant{
	
	Element e;
	Arme a;

	
	public Elfe(){
		super();
	}
	public Elfe(String nom){
		super(nom);
	}
	public Elfe(String nom,Arme a,Element e){
		super(nom);
		this.a = a;
		this.e = e;
	}

	@Override
	public Arme getArme() {
		return a;
	}

	@Override
	public void attaque(Magicien m) {
		m.subitAttaque(this);
		
	}

	@Override
	public void subitAttaque(Magicien m) {
		
		
	}

	@Override
	public Element getElement() {
		return e;
	}
	@Override
	public void attaque(Combattant c) {
		super.attaque((Vivant) c);
		
	}
	
	
	
	

}
