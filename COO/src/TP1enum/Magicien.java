package TP1enum;

public interface Magicien {

		void attaque(Magicien m);
		void subitAttaque(Magicien m);
		Element getElement();
		
}
