package TP1enum;

public class Personnage {

	private String nom;
	private int vie = 100;
	private int force = 10;
	private int numeroPersonnage;
	private static int nbrPersonnage;
	
	public String getNom(){
		return nom;
	}
	public int getVie(){
		return vie;
	}
	public int getForce(){
		return force;
	}
	public void setForce(int i){
		force = i;
	}
	public Personnage(String nom){
		this.nom = nom;
		this.numeroPersonnage = Personnage.nbrPersonnage;
		Personnage.nbrPersonnage++;
		
	}
	public Personnage(){
		this.numeroPersonnage = Personnage.nbrPersonnage;
		Personnage.nbrPersonnage++;
	}
	public void perte(int i){
		vie -= i;
	}
	public boolean estVivant(){
		return vie > 0;
	}
	public void attaque(Vivant v){
		v.perte(this.getForce());
	}
	public void subitAttaque(Vivant v){
		this.vie -= v.getForce();
	}
	@Override
	public String toString() {
		return "Personnage [nom=" + nom + ", vie=" + vie + ", force=" + force + ", numeroPersonnage=" + numeroPersonnage + "]";
	}
	
	
}
