package TP1enum;

public class Mage extends Personnage implements Magicien{

		Element e;
	
		public Mage(){
			super();
		}
		public Mage(String nom){
			super(nom);
		}
		public Mage(String nom,Element e){
			super(nom);
			this.e = e;
		}
		public void attaque(Magicien m ){
			super.attaque((Vivant) m );
		}
		@Override
		public void subitAttaque(Magicien m) {
			super.subitAttaque((Vivant) m);
			
		}
		@Override
		public Element getElement() {
			return e;
			
		}
		@Override
		public String toString() {
			return "Mage [e=" + e + ", toString()=" + super.toString() + "]";
		}
		
	
}
