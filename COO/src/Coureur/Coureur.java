package Coureur;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;

public class Coureur extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int tailleJambe11;
	int tailleJambe12;
	int tialleJambe21;
	int tailleJambe22;
	int nbrTour;

	public Coureur() {		
		this.setSize( 500, 500 );
		this.setVisible( true );
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			
		this.setLocation(500,500);
	}
	

		
	public void paint (Graphics g){
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0,0, 500, 500);
		g.setColor(Color.gray);
		g.fillOval(280,180 , 30, 40); // coureur 2 
		g.fillOval(155,135 , 40, 60); // coureur 1 
		g.setColor(Color.yellow);
		g.fillRect(275, 220, 40, 80); // coureur 2 
		g.fillRect(150, 195, 50, 100); // coureur 1 
		g.setColor(Color.BLACK);
		if(this.nbrTour%2 == 0){
			g.fillRect(277, 300, 12, 42); // jambe 1 coureur 2 
			g.fillRect(300, 300, 12, 60); // jambe 2 coureur 2
			g.fillRect(263, 223, 12, 60); // bras 1 coureur 2 
			g.fillRect(315, 223, 12, 42); // bras 1 coureur 2 
			
			g.fillRect(155, 295, 15, 50); // jambe 1 coureur 1 
			g.fillRect(180, 295, 15, 75); // jambe 2 coureur 1 
			g.fillRect(135, 197, 15, 75); // bras 1 coureur 1 
			g.fillRect(200, 197, 15, 50); // bras 2 coureur 1 
		}else{
			g.fillRect(277, 300, 12, 60); // jambe 1 coureur 2 
			g.fillRect(300, 300, 12, 42); // jambe 2 coureur 2
			g.fillRect(263, 223, 12, 42); // bras 1 coureur 2 
			g.fillRect(315, 223, 12, 60); // bras 1 coureur 2 
			
			g.fillRect(155, 295, 15, 75); // jambe 1 coureur 1 
			g.fillRect(180, 295, 15, 50); // jambe 2 coureur 1 
			g.fillRect(135, 197, 15, 50); // bras 1 coureur 1 
			g.fillRect(200, 197, 15, 75); // bras 2 coureur 1 
		}
		
	}
	public void animer() throws InterruptedException{
		while(true){
			this.repaint();
			Thread.sleep(200l);
			this.nbrTour++;
		}
	}
		
	public static void main(String[] args) throws InterruptedException {
		 Coureur c = new Coureur();
		 c.animer();
	}	
}


/*			
 			g.fillRect(277, 300, 12, 60); // jambe 1 coureur 2 
			g.fillRect(300, 300, 12, 60); // jambe 2 coureur 2
			g.fillRect(263, 223, 12, 60); // bras 1 coureur 2 
			g.fillRect(315, 223, 12, 60); // bras 1 coureur 2 
			
			g.fillRect(155, 295, 15, 75); // jambe 1 coureur 1 
			g.fillRect(180, 295, 15, 75); // jambe 2 coureur 1 
			g.fillRect(135, 197, 15, 75); // bras 1 coureur 1 
			g.fillRect(200, 197, 15, 75); // bras 2 coureur 1 
*/

