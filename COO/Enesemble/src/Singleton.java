
public class Singleton extends Ensemble{

	Integer nbr;
	
	public Singleton(int max) {
		super(max);
	}

	boolean add(int i) {
		if (i <= MAX) {
			if (nbr != null) {
				return false;
			}
			nbr = i;
			return true;
		}
		return false;

	}

	void clear() {
		nbr = null;
		
	}

	boolean remove(int i) {
		if (nbr == i) {
			nbr = null;
			return true;
		}
		return false;
	}

	boolean contains(int i) {
		if (i == nbr) {
			return true;
		}
		return false;
	}
	
}
