
public abstract class EnsembleModifiable extends Ensemble{

	public EnsembleModifiable(int max) {
		super(max);
	}
	abstract boolean add(int i);
	abstract void clear();
	abstract boolean remove(int i);
	abstract boolean contains(int i);
	
}
