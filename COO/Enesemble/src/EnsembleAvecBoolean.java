
public class EnsembleAvecBoolean extends EnsembleModifiable{

	public boolean[] tab;
	
	public EnsembleAvecBoolean(int max) {
		super(max);
		tab = new boolean[max+1];
	}

	 boolean add(int i){
		if(i <= MAX){
			if(tab[i] == true){
				return false;
			}
			tab[i]=true;
			return true;
		}
		return false;
	}
	 void clear(){
		for (int i = 0; i < tab.length; i++) {
			tab[i]=false;
		}
	}
	 boolean remove(int i){
		if(i <= MAX){
			if(!tab[i]){return false;}
			tab[i]=false;
			return true;
		}
		return false;
	}

	boolean contains(int i) {
		return tab[i];
	}

	

}
