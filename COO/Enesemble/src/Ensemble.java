	
public abstract class Ensemble {
	
	final int MAX;
	
	public Ensemble(int max){
		this.MAX = max;
	}
	
	//abstract boolean isEmpty();
	abstract boolean add(int i);
	abstract void clear();
	abstract boolean remove(int i);
	abstract boolean contains(int i);
	
	public int size(){
		int cpt = 0;
		for (int i = 0; i < MAX; i++) {
			if(this.contains(i)){
				cpt++;
			}
		}
		return cpt;
	}
	public String toString(){
		String res = "{";
		for (int i = 0; i < MAX; i++) {
			if (this.contains(i)) {
				res+= i+",";
			}
		}
		res+="}";
		return res;
	}
	public boolean isEmpty(){
		for (int i = 0; i < MAX; i++) {
			if (this.contains(i)) {
				return false;
			}
		}
		return true;
	}
	
	
	
	
}
