import java.util.ArrayList;

public class EnsembleAvecArrayList extends EnsembleModifiable{
	
	public ArrayList<Integer> l = new ArrayList<Integer>(); 

	public EnsembleAvecArrayList(int max) {
		super(max);
	}

	@Override
	boolean add(int i) {
		if (i<=20) {
			if (this.contains(i)) {
				return false;
			}
			l.add(i);
			return true;
		}
		return false;

	}

	@Override
	void clear() {
		l.clear();
		
	}

	@Override
	boolean remove(int i) {
		for (Integer nbr : l) {
			if (nbr == i) {
				l.remove(i);
				return true;
			}
		}
		return false;
		
	}

	@Override
	boolean contains(int i) {
		return l.contains(i);
	}
	
	
}
