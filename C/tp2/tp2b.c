#include <stdio.h>




int main(void){
  char c;
  int res = 0;
  c = getchar();
  while(c != EOF){
    if(c != '0' && c != '1'){
      return -1;
    }
    else if(c == '1'){
      res = res * 2;
      res += 1;
    }else if(c == '0'){
      res = res * 2;
    }
    c = getchar();
  }
  return res;
}

