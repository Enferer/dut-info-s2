#include <stdio.h>

int main(void)
{
  int p1=0;
  char c;
  while(c != EOF && p1>=0){
    c = getchar();
    if(c == '('){
      p1++;
    }
    else if(c == ')'){
      p1--;
    }
  }
  if(p1 == 0){
    return 0;
  }else{
    return 1;
  }
}
