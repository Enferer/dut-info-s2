#include <stdio.h>
int incr(int t[8]){
  int i;
  
  for(i=0;i<8;i=i+1){
    if (t[i]==1){
      t[i]=0;
    }else{
      t[i]=1;
      return -1;
    }
    
  }
  return 0;
}

void afficheTab(int tab[8]){
  int i;
  for(i=7;i>=0;i--){
    printf("%d",tab[i]);
  }
}

int puissanceDeux(int x){
  int res=1;
  int i;
  for (i=0;i<x;i++){
    res=res*2;
  }
  return res;
}


int valtab(int tab[8]){
  int i;
  int res=0;
  for(i=7;i>=0;i--){
    res=res+(tab[i]*puissanceDeux(i));
  }
  return res;
  
}
  

int main(void)
{
  int i;
  for(i=0;i<=255;i=i+1){
    int tmp=i;;
    int t[8];
    int j;
    for(j=7;j>=0;j--){
      int puissance=puissanceDeux(j);
      t[j]=tmp/puissance;
      tmp=tmp-(t[j]*puissance);
    }
    int val1=valtab(t);
    
    
    printf("Initiale: %d : ", val1);
    afficheTab(t);
    printf(",");
    int rep=incr(t);
    int val2=valtab(t);
    printf(" Calculee: %d :",val2);
    afficheTab(t);
    printf(",");
    printf(" res : (%d)", rep);

      printf(" Diagnostic: "); 
      if(val1==val2-1 && rep==-1){
	printf("Calcul correct");
      }else if(val1==255 && val2 == 0 && rep==0){
	printf("Debordement correctement detecte");
      }else{
	printf("Erreur!!");
      }
    
    printf("\n");
  }
  return 0;
}

