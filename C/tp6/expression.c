
#include "expression.h"


item* creer_diaddique(char op, item* op1, item* op2) {
	item* tmp ;
	tmp = (item*) malloc(sizeof(item)) ;
	tmp->operateur = op ;
	tmp->op1 = op1 ;
	tmp->op2 = op2 ;
	return tmp ;
}

item* creer_monaddique(char op, item* op1) {
	item* tmp ;
	tmp = (item*) malloc(sizeof(item)) ;
	tmp->operateur = op ;
	tmp->op1 = op1 ;
	tmp->op2 = NULL ;
	return tmp ;
}
item* creer_variable(char v) {
	item* tmp ;
	tmp = (item*) malloc(sizeof(item)) ;
	tmp->operateur = v ;
      
	return tmp ;
}
void inventorier_variables(item *expr) {
	if (expr->operateur == '&') {
		inventorier_variables(expr->op1) ;
		inventorier_variables(expr->op2) ;
	} else if (expr->operateur == '|') {
		inventorier_variables(expr->op1) ;
		inventorier_variables(expr->op2) ;
	} else if (expr->operateur == '^') {
		inventorier_variables(expr->op1) ;
		inventorier_variables(expr->op2) ;
	} else if (expr->operateur == '!') {
		inventorier_variables(expr->op1) ;
	} else {
		ajouter_variable(expr->operateur) ;
	}
}

void display_expression(item *expr) {
	if (expr->operateur == '&') {
		printf("(") ;
		display_expression(expr->op1) ;
		printf(" AND ") ;
		display_expression(expr->op2) ;
		printf(")") ;
	} else if (expr->operateur == '^') {
		printf("(") ;
		display_expression(expr->op1) ;
		printf(" XOR ") ;
		display_expression(expr->op2) ;
		printf(")") ;
	} else if (expr->operateur == '|') {
		printf("(") ;
		display_expression(expr->op1) ;
		printf(" OR ") ;
		display_expression(expr->op2) ;
		printf(")") ;
	} else if (expr->operateur == '!') {
		printf("( ") ;
		printf(" NOT ") ;
		display_expression(expr->op1) ;
		printf(")") ;
	} else {
		printf("[%c]", expr->operateur) ;
	}
}
int evaluer_expression(item *expr) {
	int res = 0 ;
	if (expr->operateur == '&') {
		res = (evaluer_expression(expr->op1) && evaluer_expression(expr->op2)) ;
	} else if (expr->operateur == '^') {
		res = ((evaluer_expression(expr->op1)==0) != (evaluer_expression(expr->op2)==0)) ;
	} else if (expr->operateur == '|') {
		res = (evaluer_expression(expr->op1) || evaluer_expression(expr->op2)) ;
	} else if (expr->operateur == '!') {
		res = (! evaluer_expression(expr->op1)) ;
	} else {
		res = valeur(expr->operateur) ;
	}
	return res ;
}
