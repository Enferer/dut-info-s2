#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#ifndef ITEM_H_INCLUDED
#define ITEM_H_INCLUDED
typedef struct item {
	char operateur ;
	struct item *op1 ;
	struct item *op2 ;
} item  ;
#endif
