#include "item.h"
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "expression.h"

void erreur(char *) ;
item *expression() ;
item *andOp() ;
item *orOp() ;
item *xorOp() ;
item *notOp() ;





void erreur(char *msg) {
	printf(" ERREUR: %s \n", msg) ;
	exit(1) ;
}
static char c = ' '  ;
void caractereSuivant()
{
	if (c == EOF) return ;
	do {
		c = getchar() ;
	} 
	while ((c == ' ') || (c == '\n')) ;
	}


item *expression() {
	item* resultat ;
	resultat = andOp() ;
	while (c == '|') {
		caractereSuivant() ;
		resultat = creer_diaddique( '|', resultat,andOp()) ;
	}
	return resultat ;
}

item *andOp() {
	item *resultat ;
	resultat = xorOp() ;
	while (c == '&'){
		caractereSuivant() ;
		resultat = creer_diaddique( '&', resultat,orOp()) ;
	}
	return resultat ;
}

item *orOp() {
	item *resultat ;
	resultat = xorOp() ;
	while (c == '|'){
		caractereSuivant() ;
		resultat = creer_diaddique( '|', resultat,xorOp()) ;
	}
	return resultat ;
}

item *xorOp() {
	item *resultat ;
	resultat = notOp() ;
	while (c == '^'){
		caractereSuivant() ;
		resultat = creer_diaddique( '^', resultat,notOp()) ;
	}
	return resultat ;
}

item *notOp() {
	item* resultat ;
	if (c == '!') {
		caractereSuivant() ;
		resultat = creer_monaddique('!', notOp()) ;
	} else if (isalpha(c)) {
		resultat = creer_variable(toupper(c)) ;
		caractereSuivant() ;
	} else if (c == '(') {
		caractereSuivant() ;
		resultat = expression() ;
		if (c != ')') erreur(" ')' expected.") ;
		caractereSuivant() ;
	} else {
		erreur("caractere non reconnu") ;
		resultat = NULL ;
	}
	return resultat ;
}

item* parse() {
	item *expr ;
	caractereSuivant() ;
	expr = expression() ;
	return expr ;
}
