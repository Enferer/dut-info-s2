#include <stdlib.h>
#include <stdio.h>

double f(int);

int main(int argc, char ** argv){
  int i;
  for(i=0;i<10;i++){
    printf("carre de %d : %f \n",i,f(i));
  }
  printf("\n");
  return 0;
}
