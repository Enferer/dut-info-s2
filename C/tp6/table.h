#include "item.h"
#include "expression.h"
#ifndef TABLE_H_INCLUDED
#define TABLE_H_INCLUDED
void table(item* );
void display_var_values();
void display_var_names();
void display_expr_value(item *);
#endif
