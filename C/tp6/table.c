#include "table.h"

void table(item* expr) {
	printf("\n*** TABLE DE VERITE ***\n") ;
	inventorier_variables(expr) ;				/* Etablir la liste des variables dans 'variables' */
	display_var_names() ;						/* Affiche la liste des nomes de variables */
	display_expression(expr) ;					/* Affiche l'expression �valu�e */
	printf("\n") ;

	init_valeurs() ;						/* Met toutes les variables � z�ro */
	while (next_valeurs()) {
		display_var_values() ;
		display_expr_value(expr) ;
		printf("\n") ;
	} ;			/* ligne suivante de la table de v�rit� */
}

void display_var_values() {
	char var ;
	/* Pour chaque variable : */
	init_variable() ;
	var = next_variable() ;
	while (var != '.') {
		if (valeur(var)) {
			printf(" 1 ") ;
		} else {
			printf(" 0 ") ;
		}
		var = next_variable(var) ;
	}
}
void display_var_names() {
	char var ;
	/* Pour chaque variable : */
	init_variable() ;
	var = next_variable() ;
	while (var != '.') {
		printf(" %c ", var) ;			/* Affichage du nom */
		var = next_variable(var) ;
	}
}
void display_expr_value(item *expr) {
	if (evaluer_expression(expr)) {			/* Calcule la valeur de l'expression et l'affiche */
		printf(" 1 ") ;
	} else {
		printf(" 0 ") ;
	}   
}
