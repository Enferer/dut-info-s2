
#include "item.h"
#include "expression.h"





typedef short int BOOLEAN ;
#define TRUE -1 
#define FALSE 0 

#define MAXCHAR 26

void init_variable() ;
char next_variable() ;
void init_valeurs() ;
int next_valeurs() ;
int valeur(char v) ;
void table(item *expr) ;


item* parse() ;










BOOLEAN variables[MAXCHAR] = {	FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 
								FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE };
BOOLEAN valeurs[MAXCHAR] =	{	TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE, 
								TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE,  TRUE };


void ajouter_variable(char v) {
	variables [v - 'A'] = TRUE ;
}

static int novar = -1 ;
void init_variable() { novar = -1 ; }
char next_variable() { 
	do {
		novar++ ;
	} while  ((novar < MAXCHAR) && (! variables[novar])) ;
	if (novar < MAXCHAR) {
		return ('A' + novar) ;
	} else { 
		return '.' ;
	}
}

void init_valeurs() {
int i ;
for (i = 0 ; i < MAXCHAR ; i++) 
	valeurs[i] = FALSE ;
}

int next_valeurs() {
	int i ;
	for (i = MAXCHAR ; i >= 0 ; i--) {
		if (variables[i])  { /* Variable inventori�e */
			if (valeurs[i]) {
				valeurs[i] = FALSE ;
			} else {
				valeurs[i] = TRUE ;
				return TRUE ;
			}
		}
	}
	return FALSE ;
}

int valeur(char v) {
	return valeurs[v - 'A'] ;
}









char c = ' '  ;


int main(int argc, char **argv)
{
	item* expr ;
	printf(" Donner l'expression logique � �tudier:\n") ;
	expr = parse() ;
	display_expression(expr) ;
	table(expr) ;
	return 0  ;
}
