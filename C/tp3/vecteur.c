#include <stdio.h>
#include "vecteur.h"

int norme(vecteur v){
  
  return v.x + v.y;
}

void afficher(vecteur m[]){
  int cpt = 0;
  for(cpt ; cpt < 10;cpt++){
    printf("Vecteur %d : %d , %d\n",cpt,m[cpt].x,m[cpt].y);
  }

}

int maximum(vecteur m[]){
  int cpt = 0;
  int max = 0;
  for(cpt ; cpt < 10;cpt++){
    if( norme(m[cpt]) > norme(m[max])){
      max = cpt;
    }
  }
  return max;
}


void trier(vecteur m[]){
  int boolean = 1;
  while(boolean == 1){
    int cpt = 0;
    boolean = 0;
    for(cpt;cpt < 9; cpt++){
      if(norme(m[cpt]) > norme(m[cpt+1]) ){
	vecteur temp = m[cpt];
	m[cpt] = m[cpt+1];
	m[cpt+1] = temp;
	boolean = 1;
      }
    }
  }
}

void trier2(vecteur m[], int ordre[]){
  vecteur tab [10];
  int cpt = 0;
  for(cpt ; cpt < 10 ; cpt++){
    tab[cpt] = m[ordre[cpt]];
  }
  m = tab;
}

void afficher2(vecteur m[], int ordre[]){
  int cpt = 0;
  for(cpt ; cpt < 10 ; cpt++){
     printf("Vecteur %d : %d , %d\n",cpt,m[ordre[cpt]].x,m[ordre[cpt]].y);
  }
  printf("\n");
}

void swap(vecteur** a, vecteur** b){
  // TODO
}

void trier3(vecteur* a[]){
  // TODO
}

void afficher3(vecteur* a[]){
  // TODO
}
