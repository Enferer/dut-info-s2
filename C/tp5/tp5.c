#include <stdio.h>
#include <stdlib.h>

struct cell{
	int val;
	struct cell *next; 
};

typedef struct cell cell;

typedef cell* list;

list tail(list l){
  return l->next;
}

int head(list l){
  return l->val;
}

list cons(int v, list l){
  cell *c1=(cell *)malloc(sizeof(cell));
  c1 -> val=v;
  c1 -> next=l;
  return c1;
}


int length(list l){
  cell cellule=*l;
  int compteur = 0;
  while(cellule.next != NULL){
    cell *posNext=cellule.next;
    cellule=*posNext;

    compteur++;
  }
  return compteur;
}
void print_list(list l){
  list l1 = l;
  int compteur=1;
  do{
    printf("Valeur %d , Adresse %p , Valeur : %d , Adresse suiv %p\n",compteur,&l1, l1->val , l1->next);
    compteur++;
    l1=l1->next;
  }while(l1 != NULL);
  printf("\n");
}

list append(list l1,list l2){
  list x = l1;
  
  do{
    l1 = l1->next;
  }while(l1->next != NULL);
  l1->next = &*l2;
  list list;
  list = l1;
  return list;
}


int main(){
  cell c1;
  cell c2;
  cell c3;
  
  c1.val=42;
  c1.next=&c2;
  c2.val=24;
  c2.next=&c3;
  c3.val=16;
  
  list l1;
  l1=&c1;

  // int c2val=tail(l1)->val;
   
  /*printf("fonction head : %d\n",head(l1));
  printf("fonction tail : %d\n",c2val);

  l1=cons(0,l1);
  c2val=tail(l1) -> val;
  printf("---------------------\n");
  printf("Fonction head: %d \npointer next (fonction tail ) : %p \nvaleur du next %d \n",head(l1),tail(l1),c2val);
  printf("-----------------------\n");
  printf("tail : %d\n",length(tail(l1)));
  printf("Longueur : %d\n",length(l1));
  print_list(l1);*/

  cell cb1;
  cell cb2;
  cell cb3;
  
  cb1.val=24;
  cb1.next=&cb2;
  cb2.val=42;
  cb2.next=&cb3;
  cb3.val=61;
  
  list l2;
  l2=&cb1;

  printf("-----------------------\n");
  print_list(l1);
  printf("-----------------------\n");
   print_list(l2);
  //list l = append(l1,l2);
  printf("-----------------------\n");
  //print_list(l);
  
  
  return 1;
}
